//========================================================================
/*!
\file    ICEGateway.h
\brief   ICE <--> ROS gateway node for Cobot
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#ifndef ICEGATEWAY_H_
#define ICEGATEWAY_H_

class ICEGateway {
public:
	ICEGateway();
	virtual ~ICEGateway();
};

#endif /* ICEGATEWAY_H_ */
