//========================================================================
/*!
\file    MRSMcobot.cpp
\brief   MRSM for CoBot. MRSM is a distributed module and this is the module that runs on CoBot.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#include "MRSMcobot.h"

// Speech servant
void
Speech::say(const string& message, const Ice::Current&)
{
	cout << "Speech::say() (" << message << ")\n";

	ros::NodeHandle n;
	ros::ServiceClient talkerClient = n.serviceClient<CobotTalkerSrv>("/Cobot/Talker");
	CobotTalkerSrv srv;
	std::stringstream ss;
	ss << message;
	srv.request.speechText = ss.str();
	talkerClient.call(srv);
}

// Navigation servant
Navigation::Navigation( string robotId, ros::NodeHandle *n )
{
	pthread_mutex_init(&localizationMutex, NULL);
	_n = n;

	cobotScheduler = new SchedulerProxy(robotId);
	cobotScheduler->setUser("tank_cobot");
	cobotScheduler->setPasswd("tank_cobot_passwd");
	cobotScheduler->setCookie("__utma=136847266.1036288567.1320164388.1320164388.1320164388.1; __utmz=136847266.1320164388.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=maxim%20makatchev; webstats-cmu=cmu67.165.87.210.270071320766299501; __utma=44984886.318193205.1322671669.1322870651.1323214967.3; __utmz=44984886.1323214967.3.2.utmccn=(organic)|utmcsr=google|utmctr=cmu%20shalz%20dinner|utmcmd=organic; sessionid=579e02d0109ea9fb99defeb4fdc3ca7a; csrftoken=815163a3263852899c1b435ab0648a79");
	cobotScheduler->setBaseURL("http://neontetra.coral.cs.cmu.edu/cobot/dashboard/bookings/");

	//Subscribe to localization
	localizSubscriber = _n->subscribe("/Cobot/Localization", 1, &Navigation::localizationCallback, this);
}

string
Navigation::escort( const string& room, const string& startDate, const string& startH,
		const string& startM, const string& startP, const string& endDate,
		const string& endH, const string& endM, const string& endP,
		const Ice::Current& )
{
	cout << "Navigation::escort(): " << room << "(" << startDate << "," <<
				startH << "," << startM << "," << startP << "," <<
				endDate << "," << endH << "," << endM << "," << endP << ")\n";
	return cobotScheduler->newTaskEscort(room, startDate, startH, startM, startP,
			endDate, endH, endM, endP);
}

int
Navigation::cancelTask( const string& taskId, const Ice::Current& )
{
	return cobotScheduler->cancelTask( taskId );
}

string
Navigation::goToOffice( const string& office, const string& startDate, const string& startH,
		const string& startM, const string& startP, const string& endDate,
		const string& endH, const string& endM, const string& endP,
		const Ice::Current& )
{
	cout << "Navigation::goToOffice(): " << office << "(" << startDate << "," <<
			startH << "," << startM << "," << startP << "," <<
			endDate << "," << endH << "," << endM << "," << endP << ")\n";

	return cobotScheduler->newTaskGoToRoom(office, startDate, startH, startM, startP,
			endDate, endH, endM, endP);
}

void
Navigation::goToOffices(const Offices& offices, const Ice::Current&)
{
	cout << "Navigation::goToOffice() (";

	for (unsigned i = 0; i < offices.size(); i++) {
		cout << offices[i] << " ";
	}
	cout << ")\n";
}

void
Navigation::whereAreYou(float& timeStamp, float& x, float& y,
		float& angle, float& conf, string& map, const Ice::Current& )
{
	pthread_mutex_lock(&localizationMutex);
	cout << "Navigation::whereAreYou() [" << this->timeStamp << "](" << this->x << "," <<
			this->y << "," << this->angle << ")(" << this->conf << ")(" << this->map << ")\n";

	timeStamp = this->timeStamp;
	x = this->x;
	y = this->y;
	angle = this->angle;
	conf = this->conf;
	map = this->map;
	pthread_mutex_unlock(&localizationMutex);
}

void
Navigation::localizationCallback(const cobot_msgs::CobotLocalizationMsgConstPtr& msg)
{
	pthread_mutex_lock(&localizationMutex);
	this->timeStamp = msg->timeStamp;
	this->x = msg->x;
	this->y = msg->y;
	this->angle = msg->angle;
	this->conf = msg->conf;
	this->map = msg->map;
	pthread_mutex_unlock(&localizationMutex);
}


// MRSMcobot
MRSMcobot::MRSMcobot(NodeHandle *n) {
	_n = n;
}

MRSMcobot::~MRSMcobot() {
	// TODO Auto-generated destructor stub
}

void
MRSMcobot::init(int argc, char **argv)
{
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
					("help", "produce help message")
					("Ice.Config",  po::value<string>(), "Ice configuration file")
					("robot", po::value<string>(), "set robot ID (cobot1, cobot2)");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		cout << desc << "\n";
	}

	if (vm.count("robot")) {
		_robotID = vm["robot"].as<string>();
	}

	// Set the ROS_MASTER_URI (only if this node run remotely)
	if (_robotID == "cobot1")
		putenv(strdup("ROS_MASTER_URI=http://cobot1.wv.cc.cmu.edu:11311"));
	else if (_robotID == "cobot2")
		putenv(strdup("ROS_MASTER_URI=http://cobot2.wv.cc.cmu.edu:11311"));
	else {
		cout << "Argument for robot must be cobot1 or cobot2\n";
		return;
	}

	try {
		ic = Ice::initialize(argc, argv);
		adapter = ic->createObjectAdapter( _robotID );

		navigation = new Navigation( _robotID, _n );
		speech = new Speech();

		adapter->add(navigation, ic->stringToIdentity("NavigationI"));
		adapter->add(speech, ic->stringToIdentity("SpeechI"));

		adapter->activate();
		//ic->waitForShutdown();
	} catch (const Ice::Exception& e) {
		cerr << e << endl;
	} catch (const char* msg) {
		cerr << msg << endl;
	}
}

string
MRSMcobot::officeOf( const string& personName )
{
	string office = "Unknown";

	try {
		Ice::ObjectPrx base = ic->stringToProxy( "AgendaI@CMUreceptionistAdapter" );
		CMU::AgendaIPrx agendaPrx = CMU::AgendaIPrx::checkedCast(base);
		if (!agendaPrx)
			throw "Invalid proxy";

		office = agendaPrx->officeOf( personName );

	} catch (const Ice::Exception& ex) {
		cerr << ex << endl;
	} catch (const char* msg) {
		cerr << msg << endl;
	}

	return office;
}

void
MRSMcobot::say(string message)
{
	/*	ros::NodeHandle n;
	ros::ServiceClient talkerClient = n.serviceClient<CobotTalkerSrv>("/Cobot/Talker");
	CobotTalkerSrv srv;
	std::stringstream ss;
	ss << message;
	srv.request.speechText = ss.str();
	talkerClient.call(srv);

	cout << "Speech::say() (" << message << ")\n";*/
}

string
MRSMcobot::escort( string room, string startDate, string startH, string startM, string startP,
		string endDate, string endH, string endM, string endP )
{
	//return escort( room, startDate, startH, startM, startP,
	//		endDate, endH, endM, endP );
	return navigation->cobotScheduler->newTaskEscort(room, startDate, startH, startM, startP,
			endDate, endH, endM, endP);
}

int
MRSMcobot::cancelTask( string taskId )
{
	//return cancelTask( taskId );
	return navigation->cobotScheduler->cancelTask( taskId );
}
