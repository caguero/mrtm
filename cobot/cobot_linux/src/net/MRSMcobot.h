//========================================================================
/*!
\file    MRSMcobot.h
\brief   MRSM for CoBot. MRSM is a distributed module and this is the module that runs on CoBot.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#ifndef MRSMcobot_H_
#define MRSMcobot_H_

// Interfaces available among all robots
#include "generatedIce/AgendaI.h"
#include "generatedIce/NavigationI.h"
#include "generatedIce/SpeechI.h"

#include <Ice/Ice.h>
#include <IceUtil/IceUtil.h>
#include <pthread.h>
#include <stdlib.h>
#include "SchedulerProxy.h"
#include <ros/ros.h>
#include "cobot_msgs/CobotLocalizationMsg.h"
#include "cobot_msgs/CobotTalkerSrv.h"
#include "cobot_msgs/CobotTalkerMsg.h"

//Command Line arguments
#include <boost/program_options.hpp>
namespace po = boost::program_options;

using namespace ros;
using namespace cobot_msgs;
using namespace std;
using namespace CMU;

// Navigation servant
class Navigation : public NavigationI {
public:
	Navigation( string robotId, ros::NodeHandle *n );
	string escort( const string& room, const string& startDate, const string& startH,
			const string& startM, const string& startP, const string& endDate,
			const string& endH, const string& endM, const string& endP,
			const Ice::Current& );
	int cancelTask( const string& taskId, const Ice::Current& );
	string goToOffice( const string& office, const string& startDate, const string& startH,
			const string& startM, const string& startP, const string& endDate,
			const string& endH, const string& endM, const string& endP,
			const Ice::Current& );
	void goToOffices(const Offices& offices, const Ice::Current&);
	void whereAreYou(float& timeStamp, float& x, float& y,
		  	 float& angle, float& conf, string& map, const Ice::Current& );
	SchedulerProxy *cobotScheduler;
private:
	void localizationCallback(const cobot_msgs::CobotLocalizationMsgConstPtr& msg);
	float timeStamp;
	float x;
	float y;
	float angle;
	float conf;
	string map;
	pthread_mutex_t localizationMutex;
	ros::Subscriber localizSubscriber;
	ros::NodeHandle *_n;
};

// Speech servant
class Speech : public SpeechI {
public:
	virtual void say(const string& message, const Ice::Current&);
};

class MRSMcobot {
public:
	MRSMcobot(NodeHandle *n);
	virtual ~MRSMcobot();

	void init(int argc, char **argv);
	string officeOf( const string& personName );
	string escort( string room, string startDate, string startH, string startM, string startP,
			string endDate, string endH, string endM, string endP );
	int cancelTask( string taskId );
	void say( string message );

	static void* iceServerThread(void *obj);
private:
	pthread_t tIceServer;
	SchedulerProxy *cobotScheduler;
	Navigation *navigation;
	Speech *speech;
	Ice::CommunicatorPtr ic;
	string _robotID;
	NodeHandle *_n;
	Ice::ObjectAdapterPtr adapter;
};

#endif /* MRSMcobot_H_ */
