#!/bin/sh

# Script that runs the Ice registry service for indirect proxies and start both Ice<-->ROS gateways for Cobot1 and Cobot2
# Author: Carlos Agüero (caguero@gsyc.es)
# Date: 2011/11/17

# Start Ice registry
cd ~/cobot/cobot_linux
icegridregistry --Ice.Config=./config/ice/registry.cfg&

# Start IceGateway for Cobot1
./bin/cobot_ICEGateway --Ice.Config=./config/ice/cobot1.cfg --robot=cobot1&

# Start IceGateway for Cobot2
./bin/cobot_ICEGateway --Ice.Config=./confice/ice/cobot2.cfg --robot=cobot2&
