#!/bin/sh

# Script that checks whether the Ice registry service and all Ice<-->ROS gateways are running
# Author: Carlos Agüero (caguero@gsyc.es)
# Date: 2011/11/17

# Number of processes that should be running
# registry, IceGateway (cobot1), IceGateway (cobot2)
N_ICE_PROCESSES=3

if [ `ps aux | grep "icegridregistry\|ICEGateway" | grep -v grep | wc -l` == $N_ICE_PROCESSES ]; then
	exit 1
else
	exit 0
fi
