#!/bin/sh

# Script that stops the Ice registry service and all Ice<-->ROS gateways
# Author: Carlos Agüero (caguero@gsyc.es)
# Date: 2011/11/17

# Stop Ice registry
killall icegridregistry

# Stop IceGateways
killall cobot_ICEGateway
