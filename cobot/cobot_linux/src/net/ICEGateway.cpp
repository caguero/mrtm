//========================================================================
//  This software is free: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License Version 3,
//  as published by the Free Software Foundation.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  Version 3 in the file COPYING that came with this distribution.
//  If not, see <http://www.gnu.org/licenses/>.
//========================================================================
/*!
\file    ICEGateway.cpp
\brief   ICE <--> ROS gateway node for Cobot
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#include <iostream>
#include <stdio.h>
#include "terminal_utils.h"
#include "proghelp.h"
#include <ros/ros.h>
#include "MRSMcobot.h"

bool run = true;

int main(int argc, char **argv) {

	printf("\nCoBot ICEGateway Node\n\n");

	ros::init(argc, argv, "Cobot_ICEGateway");
	ros::NodeHandle n;

	ColourTerminal(TerminalUtils::TERMINAL_COL_WHITE,TerminalUtils::TERMINAL_COL_BLACK,TerminalUtils::TERMINAL_ATTR_BRIGHT);
	ResetTerminal();

	// main program initialization
	InitHandleStop(&run);

	MRSMcobot *mrsm = new MRSMcobot(&n);
	mrsm->init( argc, argv );

	ros::spin();
	return(0);
}
