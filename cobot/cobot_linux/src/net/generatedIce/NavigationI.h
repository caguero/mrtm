// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1
// Generated from file `NavigationI.ice'

#ifndef _____cpp__NavigationI_h__
#define _____cpp__NavigationI_h__

#include <Ice/LocalObjectF.h>
#include <Ice/ProxyF.h>
#include <Ice/ObjectF.h>
#include <Ice/Exception.h>
#include <Ice/LocalObject.h>
#include <Ice/Proxy.h>
#include <Ice/Object.h>
#include <Ice/Outgoing.h>
#include <Ice/Incoming.h>
#include <Ice/Direct.h>
#include <Ice/StreamF.h>
#include <Common.h>
#include <Ice/UndefSysMacros.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 303
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 1
#       error Ice patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace CMU
{

class NavigationI;

}

}

namespace CMU
{

class NavigationI;
bool operator==(const NavigationI&, const NavigationI&);
bool operator<(const NavigationI&, const NavigationI&);

}

namespace IceInternal
{

::Ice::Object* upCast(::CMU::NavigationI*);
::IceProxy::Ice::Object* upCast(::IceProxy::CMU::NavigationI*);

}

namespace CMU
{

typedef ::IceInternal::Handle< ::CMU::NavigationI> NavigationIPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::CMU::NavigationI> NavigationIPrx;

void __read(::IceInternal::BasicStream*, NavigationIPrx&);
void __patch__NavigationIPtr(void*, ::Ice::ObjectPtr&);

}

namespace IceProxy
{

namespace CMU
{

class NavigationI : virtual public ::IceProxy::Ice::Object
{
public:

    void whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, 0);
    }
    void whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Context& __ctx)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, &__ctx);
    }
    
private:

    void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Context*);
    
public:

    void goToOffices(const ::CMU::Offices& officeList)
    {
        goToOffices(officeList, 0);
    }
    void goToOffices(const ::CMU::Offices& officeList, const ::Ice::Context& __ctx)
    {
        goToOffices(officeList, &__ctx);
    }
    
private:

    void goToOffices(const ::CMU::Offices&, const ::Ice::Context*);
    
public:

    ::std::string goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, 0);
    }
    ::std::string goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context& __ctx)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, &__ctx);
    }
    
private:

    ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:

    ::std::string escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, 0);
    }
    ::std::string escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context& __ctx)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, &__ctx);
    }
    
private:

    ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:

    ::Ice::Int cancelTask(const ::std::string& taskId)
    {
        return cancelTask(taskId, 0);
    }
    ::Ice::Int cancelTask(const ::std::string& taskId, const ::Ice::Context& __ctx)
    {
        return cancelTask(taskId, &__ctx);
    }
    
private:

    ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Context*);
    
public:
    
    ::IceInternal::ProxyHandle<NavigationI> ice_context(const ::Ice::Context& __context) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_context(__context).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_context(__context).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_adapterId(const std::string& __id) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_adapterId(__id).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_endpoints(const ::Ice::EndpointSeq& __endpoints) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_endpoints(__endpoints).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_endpoints(__endpoints).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_locatorCacheTimeout(int __timeout) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_locatorCacheTimeout(__timeout).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_locatorCacheTimeout(__timeout).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_connectionCached(bool __cached) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_connectionCached(__cached).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_connectionCached(__cached).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_endpointSelection(::Ice::EndpointSelectionType __est) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_endpointSelection(__est).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_endpointSelection(__est).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_secure(bool __secure) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_secure(__secure).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_preferSecure(bool __preferSecure) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_preferSecure(__preferSecure).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_preferSecure(__preferSecure).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_router(const ::Ice::RouterPrx& __router) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_router(__router).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_router(__router).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_locator(__locator).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_collocationOptimized(bool __co) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_collocationOptimized(__co).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_collocationOptimized(__co).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_twoway() const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_twoway().get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_twoway().get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_oneway() const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_oneway().get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_oneway().get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_batchOneway() const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_batchOneway().get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_datagram() const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_datagram().get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_datagram().get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_batchDatagram() const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_batchDatagram().get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_compress(bool __compress) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_compress(__compress).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_compress(__compress).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_timeout(int __timeout) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_timeout(__timeout).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    #endif
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_connectionId(const std::string& __id) const
    {
    #if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
        typedef ::IceProxy::Ice::Object _Base;
        return dynamic_cast<NavigationI*>(_Base::ice_connectionId(__id).get());
    #else
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_connectionId(__id).get());
    #endif
    }
    
    static const ::std::string& ice_staticId();

private: 

    virtual ::IceInternal::Handle< ::IceDelegateM::Ice::Object> __createDelegateM();
    virtual ::IceInternal::Handle< ::IceDelegateD::Ice::Object> __createDelegateD();
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

namespace IceDelegate
{

namespace CMU
{

class NavigationI : virtual public ::IceDelegate::Ice::Object
{
public:

    virtual void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Context*) = 0;

    virtual void goToOffices(const ::CMU::Offices&, const ::Ice::Context*) = 0;

    virtual ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*) = 0;

    virtual ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*) = 0;

    virtual ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Context*) = 0;
};

}

}

namespace IceDelegateM
{

namespace CMU
{

class NavigationI : virtual public ::IceDelegate::CMU::NavigationI,
                    virtual public ::IceDelegateM::Ice::Object
{
public:

    virtual void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Context*);

    virtual void goToOffices(const ::CMU::Offices&, const ::Ice::Context*);

    virtual ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

    virtual ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

    virtual ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Context*);
};

}

}

namespace IceDelegateD
{

namespace CMU
{

class NavigationI : virtual public ::IceDelegate::CMU::NavigationI,
                    virtual public ::IceDelegateD::Ice::Object
{
public:

    virtual void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Context*);

    virtual void goToOffices(const ::CMU::Offices&, const ::Ice::Context*);

    virtual ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

    virtual ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);

    virtual ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Context*);
};

}

}

namespace CMU
{

class NavigationI : virtual public ::Ice::Object
{
public:

    typedef NavigationIPrx ProxyType;
    typedef NavigationIPtr PointerType;
    
    virtual ::Ice::ObjectPtr ice_clone() const;

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
    ::Ice::DispatchStatus ___whereAreYou(::IceInternal::Incoming&, const ::Ice::Current&);

    virtual void goToOffices(const ::CMU::Offices&, const ::Ice::Current& = ::Ice::Current()) = 0;
    ::Ice::DispatchStatus ___goToOffices(::IceInternal::Incoming&, const ::Ice::Current&);

    virtual ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
    ::Ice::DispatchStatus ___goToOffice(::IceInternal::Incoming&, const ::Ice::Current&);

    virtual ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
    ::Ice::DispatchStatus ___escort(::IceInternal::Incoming&, const ::Ice::Current&);

    virtual ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
    ::Ice::DispatchStatus ___cancelTask(::IceInternal::Incoming&, const ::Ice::Current&);

    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
    virtual void __write(const ::Ice::OutputStreamPtr&) const;
    virtual void __read(const ::Ice::InputStreamPtr&, bool);
};

}

#endif
