// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1
// Generated from file `SpeechI.ice'

#include <SpeechI.h>
#include <Ice/LocalException.h>
#include <Ice/ObjectFactory.h>
#include <Ice/BasicStream.h>
#include <IceUtil/Iterator.h>
#include <IceUtil/ScopedArray.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 303
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 1
#       error Ice patch level mismatch!
#   endif
#endif

static const ::std::string __CMU__SpeechI__say_name = "say";

::Ice::Object* IceInternal::upCast(::CMU::SpeechI* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::CMU::SpeechI* p) { return p; }

void
CMU::__read(::IceInternal::BasicStream* __is, ::CMU::SpeechIPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::CMU::SpeechI;
        v->__copyFrom(proxy);
    }
}

void
IceProxy::CMU::SpeechI::say(const ::std::string& message, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::SpeechI* __del = dynamic_cast< ::IceDelegate::CMU::SpeechI*>(__delBase.get());
            __del->say(message, __ctx);
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

const ::std::string&
IceProxy::CMU::SpeechI::ice_staticId()
{
    return ::CMU::SpeechI::ice_staticId();
}

::IceInternal::Handle< ::IceDelegateM::Ice::Object>
IceProxy::CMU::SpeechI::__createDelegateM()
{
    return ::IceInternal::Handle< ::IceDelegateM::Ice::Object>(new ::IceDelegateM::CMU::SpeechI);
}

::IceInternal::Handle< ::IceDelegateD::Ice::Object>
IceProxy::CMU::SpeechI::__createDelegateD()
{
    return ::IceInternal::Handle< ::IceDelegateD::Ice::Object>(new ::IceDelegateD::CMU::SpeechI);
}

::IceProxy::Ice::Object*
IceProxy::CMU::SpeechI::__newInstance() const
{
    return new SpeechI;
}

void
IceDelegateM::CMU::SpeechI::say(const ::std::string& message, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__SpeechI__say_name, ::Ice::Normal, __context);
    try
    {
        ::IceInternal::BasicStream* __os = __og.os();
        __os->write(message);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    bool __ok = __og.invoke();
    if(!__og.is()->b.empty())
    {
        try
        {
            if(!__ok)
            {
                try
                {
                    __og.throwUserException();
                }
                catch(const ::Ice::UserException& __ex)
                {
                    ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                    throw __uue;
                }
            }
            __og.is()->skipEmptyEncaps();
        }
        catch(const ::Ice::LocalException& __ex)
        {
            throw ::IceInternal::LocalExceptionWrapper(__ex, false);
        }
    }
}

void
IceDelegateD::CMU::SpeechI::say(const ::std::string& message, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(const ::std::string& message, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _m_message(message)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::SpeechI* servant = dynamic_cast< ::CMU::SpeechI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            servant->say(_m_message, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        const ::std::string& _m_message;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__SpeechI__say_name, ::Ice::Normal, __context);
    try
    {
        _DirectI __direct(message, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
}

::Ice::ObjectPtr
CMU::SpeechI::ice_clone() const
{
    throw ::Ice::CloneNotImplementedException(__FILE__, __LINE__);
    return 0; // to avoid a warning with some compilers
}

static const ::std::string __CMU__SpeechI_ids[2] =
{
    "::CMU::SpeechI",
    "::Ice::Object"
};

bool
CMU::SpeechI::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__CMU__SpeechI_ids, __CMU__SpeechI_ids + 2, _s);
}

::std::vector< ::std::string>
CMU::SpeechI::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__CMU__SpeechI_ids[0], &__CMU__SpeechI_ids[2]);
}

const ::std::string&
CMU::SpeechI::ice_id(const ::Ice::Current&) const
{
    return __CMU__SpeechI_ids[0];
}

const ::std::string&
CMU::SpeechI::ice_staticId()
{
    return __CMU__SpeechI_ids[0];
}

::Ice::DispatchStatus
CMU::SpeechI::___say(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    __is->startReadEncaps();
    ::std::string message;
    __is->read(message);
    __is->endReadEncaps();
    say(message, __current);
    return ::Ice::DispatchOK;
}

static ::std::string __CMU__SpeechI_all[] =
{
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping",
    "say"
};

::Ice::DispatchStatus
CMU::SpeechI::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__CMU__SpeechI_all, __CMU__SpeechI_all + 5, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __CMU__SpeechI_all)
    {
        case 0:
        {
            return ___ice_id(in, current);
        }
        case 1:
        {
            return ___ice_ids(in, current);
        }
        case 2:
        {
            return ___ice_isA(in, current);
        }
        case 3:
        {
            return ___ice_ping(in, current);
        }
        case 4:
        {
            return ___say(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
CMU::SpeechI::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__write(__os);
#else
    ::Ice::Object::__write(__os);
#endif
}

void
CMU::SpeechI::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__read(__is, true);
#else
    ::Ice::Object::__read(__is, true);
#endif
}

void
CMU::SpeechI::__write(const ::Ice::OutputStreamPtr&) const
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type CMU::SpeechI was not generated with stream support";
    throw ex;
}

void
CMU::SpeechI::__read(const ::Ice::InputStreamPtr&, bool)
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type CMU::SpeechI was not generated with stream support";
    throw ex;
}

void 
CMU::__patch__SpeechIPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::CMU::SpeechIPtr* p = static_cast< ::CMU::SpeechIPtr*>(__addr);
    assert(p);
    *p = ::CMU::SpeechIPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::CMU::SpeechI::ice_staticId(), v->ice_id());
    }
}

bool
CMU::operator==(const ::CMU::SpeechI& l, const ::CMU::SpeechI& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
CMU::operator<(const ::CMU::SpeechI& l, const ::CMU::SpeechI& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}
