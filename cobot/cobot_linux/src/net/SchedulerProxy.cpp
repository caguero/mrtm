//========================================================================
/*!
\file    SchedulerProxy.cpp
\brief   Class that communicates with the web server to schedule tasks on CoBot. It uses curl for HTTP comms.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#include "SchedulerProxy.h"

SchedulerProxy::SchedulerProxy( string robotID ) {
	curl_global_init(CURL_GLOBAL_ALL);
	_robotID = robotID;
}

SchedulerProxy::~SchedulerProxy() {

}

void
SchedulerProxy::setUser( string user )
{
	this->_user = user;
}

string
SchedulerProxy::getUser()
{
	return _user;
}

void
SchedulerProxy::setPasswd( string passwd )
{
	this->_passwd = passwd;
}

string
SchedulerProxy::getPasswd()
{
	return _passwd;
}

void
SchedulerProxy::setCookie( string cookie )
{
	this->_cookie = cookie;
}

string
SchedulerProxy::getCookie()
{
	return _cookie;
}

void
SchedulerProxy::setBaseURL( string url)
{
	this->_baseURL = url;
}

string
SchedulerProxy::getBaseURL()
{
	return _baseURL;
}

int
writer(char *data, size_t size, size_t nmemb, string *buffer){
	int result = 0;
	if(buffer != NULL) {
		buffer -> append(data, size * nmemb);
		result = size * nmemb;
	}
	return result;
}

string
SchedulerProxy::newTaskEscort( string room, string startDate, string startH, string startM, string startP,
		string endDate, string endH, string endM, string endP )
{
	reply = "";
	string task_id = "NoId";

	curl = curl_easy_init();

	if(curl) {
		/* what URL that receives this POST */
		curl_easy_setopt(curl, CURLOPT_URL, "http://neontetra.coral.cs.cmu.edu/cobot/dashboard/scheduler1/submit/");
		string data = "csrfmiddlewaretoken=815163a3263852899c1b435ab0648a79&task=Escort&room=" + room + "&robot=" + _robotID + "&model=w&date1=" + startDate + "&time1h=" + startH + "&time1m=" + startM + "&time1p=" + startP +
				"&date2=" + endDate + "&time2h=" + endH + "&time2m=" + endM + "&time2p=" + endP;
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);	/* Function Pointer "writer" manages the required buffer size */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &reply ); 	/* Data Pointer &buffer stores downloaded web content */
		curl_easy_setopt(curl, CURLOPT_COOKIE, getCookie().c_str());
		res = curl_easy_perform(curl);

		//cout << reply << endl;

		task_id = getTaskId( reply );

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	return task_id;
}

string
SchedulerProxy::newTaskGoToRoom( string room, string startDate, string startH, string startM, string startP,
		string endDate, string endH, string endM, string endP )
{
	reply = "";
	string task_id = "NoId";

	curl = curl_easy_init();

	if(curl) {
		/* what URL that receives this POST */
		curl_easy_setopt(curl, CURLOPT_URL, "http://neontetra.coral.cs.cmu.edu/cobot/dashboard/scheduler1/submit/");
		string data = "csrfmiddlewaretoken=815163a3263852899c1b435ab0648a79&task=GoToRoom&room=" + room + "&robot=" + _robotID + "&model=w&date1=" + startDate + "&time1h=" + startH + "&time1m=" + startM + "&time1p=" + startP +
				"&date2=" + endDate + "&time2h=" + endH + "&time2m=" + endM + "&time2p=" + endP;
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);	/* Function Pointer "writer" manages the required buffer size */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &reply ); 	/* Data Pointer &buffer stores downloaded web content */
		curl_easy_setopt(curl, CURLOPT_COOKIE, getCookie().c_str());
		res = curl_easy_perform(curl);

		//cout << reply << endl;

		task_id = getTaskId( reply );

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	return task_id;
}

string
SchedulerProxy::getTaskId(string httpResponse)
{
	string taskId;
	string startCancelPattern = "/cobot/dashboard/bookings/delete/";
	string endCancelPattern = "/\">Cancel your request</a>";
	size_t taskIdStartPos = httpResponse.find(startCancelPattern) + startCancelPattern.length();
	size_t taskIdEndPos = httpResponse.find(endCancelPattern);
	size_t taskIdLength = taskIdEndPos - taskIdStartPos;
	if (httpResponse.find(startCancelPattern) != string::npos) {
		taskId = httpResponse.substr(taskIdStartPos, taskIdLength);
		//cout << "(" << taskId << ")\n";
		return taskId;
	} else
		return "NoId";
}

//ToDo: Check that task has been removed
int
SchedulerProxy::cancelTask( string taskId )
{
	reply = "";
	string cancelTask_URL = getBaseURL() + "delete/" + taskId + "/";

	curl = curl_easy_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, cancelTask_URL.c_str());
		curl_easy_setopt(curl, CURLOPT_COOKIE, getCookie().c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);	/* Function Pointer "writer" manages the required buffer size */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &reply ); 	/* Data Pointer &buffer stores downloaded web content */
		res = curl_easy_perform(curl);

		/* always cleanup */
		curl_easy_cleanup(curl);

		return CANCEL_TASK_SUCCESS;
	}

	return CANCEL_TASK_FAIL;
}
