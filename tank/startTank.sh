#!/bin/sh

#central
gnome-terminal -t ipc --geometry 62x8+1+600 -e "bash -c \"source /home/carlos/.bashrc; source /home/carlos/tank/rchallenge/externalenv.sh; source /home/carlos/tank/rchallenge/chalenv.sh; cd ~/tank/rchallenge; export CENTRALHOST=localhost:1381; ./external/ipc/bin/Linux-2.6/central; exec bash\""
sleep 2 

#expression
gnome-terminal -t expression --geometry 91x8+520+380 -e "bash -c \"source /home/carlos/.bashrc; source /home/carlos/tank/rchallenge/externalenv.sh; source /home/carlos/tank/rchallenge/chalenv.sh; cd ~/tank/rchallenge; export CENTRALHOST=localhost:1381; ./src/head/expression/bin/expression -s -l -r /home/carlos/tank/rchallenge/src/head/scripts/tank/tank.year1.expr; exec bash\""

#robocept_interact
gnome-terminal -t robocept_interact --geometry 91x17+520+1 -e "bash -c \"source /home/carlos/.bashrc; source /home/carlos/tank/rchallenge/externalenv.sh; source /home/carlos/tank/rchallenge/chalenv.sh; cd ~/tank/rchallenge; export CENTRALHOST=localhost:1381; ./src/tasks/roboceptionist/bin/robocept_interact; exec bash\""

#give_directions
gnome-terminal -t give_directions --geometry 91x8+520+600 -e "bash -c \"source /home/carlos/.bashrc; source /home/carlos/tank/rchallenge/externalenv.sh; source /home/carlos/tank/rchallenge/chalenv.sh; cd ~/tank/rchallenge; export CENTRALHOST=localhost:1381; ./bin/giveDirections /home/carlos/tank/rchallenge/src/tasks/give_directions; exec bash\""
