// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `NavigationI.ice'

#ifndef _____cpp_nao__NavigationI_h__
#define _____cpp_nao__NavigationI_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#include </home/carlos/nao/MRSM_interfaces/build/Common.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace CMU
{

class NavigationI;

}

}

namespace CMU
{

class NavigationI;
bool operator==(const NavigationI&, const NavigationI&);
bool operator<(const NavigationI&, const NavigationI&);

}

namespace IceInternal
{

::Ice::Object* upCast(::CMU::NavigationI*);
::IceProxy::Ice::Object* upCast(::IceProxy::CMU::NavigationI*);

}

namespace CMU
{

typedef ::IceInternal::Handle< ::CMU::NavigationI> NavigationIPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::CMU::NavigationI> NavigationIPrx;

void __read(::IceInternal::BasicStream*, NavigationIPrx&);
void __patch__NavigationIPtr(void*, ::Ice::ObjectPtr&);

}

namespace CMU
{

}

namespace CMU
{

class NavigationI : virtual public ::Ice::Object
{
public:

    typedef NavigationIPrx ProxyType;
    typedef NavigationIPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___whereAreYou(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void goToOffices(const ::CMU::Offices&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___goToOffices(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___goToOffice(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___escort(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___cancelTask(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace IceProxy
{

namespace CMU
{

class NavigationI : virtual public ::IceProxy::Ice::Object
{
public:

    void whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, 0);
    }
    void whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Context& __ctx)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, &__ctx);
    }
    
private:

    void whereAreYou(::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::Ice::Float&, ::std::string&, const ::Ice::Context*);
    
public:

    void goToOffices(const ::CMU::Offices& officeList)
    {
        goToOffices(officeList, 0);
    }
    void goToOffices(const ::CMU::Offices& officeList, const ::Ice::Context& __ctx)
    {
        goToOffices(officeList, &__ctx);
    }
    
private:

    void goToOffices(const ::CMU::Offices&, const ::Ice::Context*);
    
public:

    ::std::string goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, 0);
    }
    ::std::string goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context& __ctx)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, &__ctx);
    }
    
private:

    ::std::string goToOffice(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:

    ::std::string escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, 0);
    }
    ::std::string escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context& __ctx)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, &__ctx);
    }
    
private:

    ::std::string escort(const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:

    ::Ice::Int cancelTask(const ::std::string& taskId)
    {
        return cancelTask(taskId, 0);
    }
    ::Ice::Int cancelTask(const ::std::string& taskId, const ::Ice::Context& __ctx)
    {
        return cancelTask(taskId, &__ctx);
    }
    
private:

    ::Ice::Int cancelTask(const ::std::string&, const ::Ice::Context*);
    
public:
    
    ::IceInternal::ProxyHandle<NavigationI> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_secure(bool __secure) const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<NavigationI> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<NavigationI> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<NavigationI> ice_twoway() const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_oneway() const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_batchOneway() const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_datagram() const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_batchDatagram() const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<NavigationI> ice_timeout(int __timeout) const
    {
        return dynamic_cast<NavigationI*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
