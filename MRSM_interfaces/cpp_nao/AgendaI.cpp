// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `AgendaI.ice'

#include <AgendaI.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __CMU__AgendaI__officeOf_name = "officeOf";

::Ice::Object* IceInternal::upCast(::CMU::AgendaI* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::CMU::AgendaI* p) { return p; }

void
CMU::__read(::IceInternal::BasicStream* __is, ::CMU::AgendaIPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::CMU::AgendaI;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __CMU__AgendaI_ids[2] =
{
    "::CMU::AgendaI",
    "::Ice::Object"
};

bool
CMU::AgendaI::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__CMU__AgendaI_ids, __CMU__AgendaI_ids + 2, _s);
}

::std::vector< ::std::string>
CMU::AgendaI::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__CMU__AgendaI_ids[0], &__CMU__AgendaI_ids[2]);
}

const ::std::string&
CMU::AgendaI::ice_id(const ::Ice::Current&) const
{
    return __CMU__AgendaI_ids[0];
}

const ::std::string&
CMU::AgendaI::ice_staticId()
{
    return __CMU__AgendaI_ids[0];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::AgendaI::___officeOf(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string personName;
    __is->read(personName);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = officeOf(personName, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __CMU__AgendaI_all[] =
{
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping",
    "officeOf"
};

::Ice::DispatchStatus
CMU::AgendaI::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__CMU__AgendaI_all, __CMU__AgendaI_all + 5, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __CMU__AgendaI_all)
    {
        case 0:
        {
            return ___ice_id(in, current);
        }
        case 1:
        {
            return ___ice_ids(in, current);
        }
        case 2:
        {
            return ___ice_isA(in, current);
        }
        case 3:
        {
            return ___ice_ping(in, current);
        }
        case 4:
        {
            return ___officeOf(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
CMU::AgendaI::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
CMU::AgendaI::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
CMU::operator==(const ::CMU::AgendaI& l, const ::CMU::AgendaI& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
CMU::operator<(const ::CMU::AgendaI& l, const ::CMU::AgendaI& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
CMU::__patch__AgendaIPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::CMU::AgendaIPtr* p = static_cast< ::CMU::AgendaIPtr*>(__addr);
    assert(p);
    *p = ::CMU::AgendaIPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::CMU::AgendaI::ice_staticId(), v->ice_id());
    }
}

::std::string
IceProxy::CMU::AgendaI::officeOf(const ::std::string& personName, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__CMU__AgendaI__officeOf_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__AgendaI__officeOf_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(personName);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::std::string __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

const ::std::string&
IceProxy::CMU::AgendaI::ice_staticId()
{
    return __CMU__AgendaI_ids[0];
}

::IceProxy::Ice::Object*
IceProxy::CMU::AgendaI::__newInstance() const
{
    return new AgendaI;
}
