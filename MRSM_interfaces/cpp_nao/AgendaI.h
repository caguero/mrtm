// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `AgendaI.ice'

#ifndef _____cpp_nao__AgendaI_h__
#define _____cpp_nao__AgendaI_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace CMU
{

class AgendaI;

}

}

namespace CMU
{

class AgendaI;
bool operator==(const AgendaI&, const AgendaI&);
bool operator<(const AgendaI&, const AgendaI&);

}

namespace IceInternal
{

::Ice::Object* upCast(::CMU::AgendaI*);
::IceProxy::Ice::Object* upCast(::IceProxy::CMU::AgendaI*);

}

namespace CMU
{

typedef ::IceInternal::Handle< ::CMU::AgendaI> AgendaIPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::CMU::AgendaI> AgendaIPrx;

void __read(::IceInternal::BasicStream*, AgendaIPrx&);
void __patch__AgendaIPtr(void*, ::Ice::ObjectPtr&);

}

namespace CMU
{

}

namespace CMU
{

class AgendaI : virtual public ::Ice::Object
{
public:

    typedef AgendaIPrx ProxyType;
    typedef AgendaIPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual ::std::string officeOf(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___officeOf(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace IceProxy
{

namespace CMU
{

class AgendaI : virtual public ::IceProxy::Ice::Object
{
public:

    ::std::string officeOf(const ::std::string& personName)
    {
        return officeOf(personName, 0);
    }
    ::std::string officeOf(const ::std::string& personName, const ::Ice::Context& __ctx)
    {
        return officeOf(personName, &__ctx);
    }
    
private:

    ::std::string officeOf(const ::std::string&, const ::Ice::Context*);
    
public:
    
    ::IceInternal::ProxyHandle<AgendaI> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_secure(bool __secure) const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<AgendaI> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<AgendaI> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<AgendaI> ice_twoway() const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_oneway() const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_batchOneway() const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_datagram() const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_batchDatagram() const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<AgendaI> ice_timeout(int __timeout) const
    {
        return dynamic_cast<AgendaI*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
