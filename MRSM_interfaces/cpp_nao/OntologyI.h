// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `OntologyI.ice'

#ifndef _____cpp_nao__OntologyI_h__
#define _____cpp_nao__OntologyI_h__

#include <IceE/ProxyF.h>
#include <IceE/ObjectF.h>
#include <IceE/Exception.h>
#include <IceE/ScopedArray.h>
#include <IceE/Proxy.h>
#include <IceE/Object.h>
#ifndef ICEE_PURE_CLIENT
#  include <IceE/Incoming.h>
#endif
#include <IceE/Outgoing.h>
#include </home/carlos/nao/MRSM_interfaces/build/Common.h>
#include <IceE/UndefSysMacros.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

namespace IceProxy
{

namespace CMU
{

class OntologyI;

}

}

namespace CMU
{

class OntologyI;
bool operator==(const OntologyI&, const OntologyI&);
bool operator<(const OntologyI&, const OntologyI&);

}

namespace IceInternal
{

::Ice::Object* upCast(::CMU::OntologyI*);
::IceProxy::Ice::Object* upCast(::IceProxy::CMU::OntologyI*);

}

namespace CMU
{

typedef ::IceInternal::Handle< ::CMU::OntologyI> OntologyIPtr;
typedef ::IceInternal::ProxyHandle< ::IceProxy::CMU::OntologyI> OntologyIPrx;

void __read(::IceInternal::BasicStream*, OntologyIPrx&);
void __patch__OntologyIPtr(void*, ::Ice::ObjectPtr&);

}

namespace CMU
{

}

namespace CMU
{

class OntologyI : virtual public ::Ice::Object
{
public:

    typedef OntologyIPrx ProxyType;
    typedef OntologyIPtr PointerType;
    

    virtual bool ice_isA(const ::std::string&, const ::Ice::Current& = ::Ice::Current()) const;
    virtual ::std::vector< ::std::string> ice_ids(const ::Ice::Current& = ::Ice::Current()) const;
    virtual const ::std::string& ice_id(const ::Ice::Current& = ::Ice::Current()) const;
    static const ::std::string& ice_staticId();

    virtual ::CMU::Offices getCurrentPath(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___getCurrentPath(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void setMyStartingPoint(const ::std::string&, const ::std::string&, const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___setMyStartingPoint(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::CMU::Topics getInterests(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___getInterests(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual ::std::string getCurrentContext(const ::Ice::Current& = ::Ice::Current()) = 0;
#ifndef ICEE_PURE_CLIENT
    ::Ice::DispatchStatus ___getCurrentContext(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
    virtual ::Ice::DispatchStatus __dispatch(::IceInternal::Incoming&, const ::Ice::Current&);
#endif // ICEE_PURE_CLIENT

    virtual void __write(::IceInternal::BasicStream*) const;
    virtual void __read(::IceInternal::BasicStream*, bool);
};

}

namespace IceProxy
{

namespace CMU
{

class OntologyI : virtual public ::IceProxy::Ice::Object
{
public:

    ::CMU::Offices getCurrentPath()
    {
        return getCurrentPath(0);
    }
    ::CMU::Offices getCurrentPath(const ::Ice::Context& __ctx)
    {
        return getCurrentPath(&__ctx);
    }
    
private:

    ::CMU::Offices getCurrentPath(const ::Ice::Context*);
    
public:

    void setMyStartingPoint(const ::std::string& robotId, const ::std::string& office)
    {
        setMyStartingPoint(robotId, office, 0);
    }
    void setMyStartingPoint(const ::std::string& robotId, const ::std::string& office, const ::Ice::Context& __ctx)
    {
        setMyStartingPoint(robotId, office, &__ctx);
    }
    
private:

    void setMyStartingPoint(const ::std::string&, const ::std::string&, const ::Ice::Context*);
    
public:

    ::CMU::Topics getInterests()
    {
        return getInterests(0);
    }
    ::CMU::Topics getInterests(const ::Ice::Context& __ctx)
    {
        return getInterests(&__ctx);
    }
    
private:

    ::CMU::Topics getInterests(const ::Ice::Context*);
    
public:

    ::std::string getCurrentContext()
    {
        return getCurrentContext(0);
    }
    ::std::string getCurrentContext(const ::Ice::Context& __ctx)
    {
        return getCurrentContext(&__ctx);
    }
    
private:

    ::std::string getCurrentContext(const ::Ice::Context*);
    
public:
    
    ::IceInternal::ProxyHandle<OntologyI> ice_context(const ::Ice::Context& __context) const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_context(__context).get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_secure(bool __secure) const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_secure(__secure).get());
    }
    
#ifdef ICEE_HAS_ROUTER
    ::IceInternal::ProxyHandle<OntologyI> ice_router(const ::Ice::RouterPrx& __router) const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_router(__router).get());
    }
#endif // ICEE_HAS_ROUTER
    
#ifdef ICEE_HAS_LOCATOR
    ::IceInternal::ProxyHandle<OntologyI> ice_locator(const ::Ice::LocatorPrx& __locator) const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_locator(__locator).get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_adapterId(const std::string& __id) const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_adapterId(__id).get());
    }
#endif // ICEE_HAS_LOCATOR
    
    ::IceInternal::ProxyHandle<OntologyI> ice_twoway() const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_twoway().get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_oneway() const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_oneway().get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_batchOneway() const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_batchOneway().get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_datagram() const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_datagram().get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_batchDatagram() const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_batchDatagram().get());
    }
    
    ::IceInternal::ProxyHandle<OntologyI> ice_timeout(int __timeout) const
    {
        return dynamic_cast<OntologyI*>(::IceProxy::Ice::Object::ice_timeout(__timeout).get());
    }
    
    static const ::std::string& ice_staticId();
    
private:
    
    virtual ::IceProxy::Ice::Object* __newInstance() const;
};

}

}

#endif
