// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `SpeechI.ice'

#include <SpeechI.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __CMU__SpeechI__say_name = "say";

::Ice::Object* IceInternal::upCast(::CMU::SpeechI* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::CMU::SpeechI* p) { return p; }

void
CMU::__read(::IceInternal::BasicStream* __is, ::CMU::SpeechIPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::CMU::SpeechI;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __CMU__SpeechI_ids[2] =
{
    "::CMU::SpeechI",
    "::Ice::Object"
};

bool
CMU::SpeechI::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__CMU__SpeechI_ids, __CMU__SpeechI_ids + 2, _s);
}

::std::vector< ::std::string>
CMU::SpeechI::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__CMU__SpeechI_ids[0], &__CMU__SpeechI_ids[2]);
}

const ::std::string&
CMU::SpeechI::ice_id(const ::Ice::Current&) const
{
    return __CMU__SpeechI_ids[0];
}

const ::std::string&
CMU::SpeechI::ice_staticId()
{
    return __CMU__SpeechI_ids[0];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::SpeechI::___say(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string message;
    __is->read(message);
    say(message, __current);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __CMU__SpeechI_all[] =
{
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping",
    "say"
};

::Ice::DispatchStatus
CMU::SpeechI::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__CMU__SpeechI_all, __CMU__SpeechI_all + 5, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __CMU__SpeechI_all)
    {
        case 0:
        {
            return ___ice_id(in, current);
        }
        case 1:
        {
            return ___ice_ids(in, current);
        }
        case 2:
        {
            return ___ice_isA(in, current);
        }
        case 3:
        {
            return ___ice_ping(in, current);
        }
        case 4:
        {
            return ___say(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
CMU::SpeechI::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
CMU::SpeechI::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
CMU::operator==(const ::CMU::SpeechI& l, const ::CMU::SpeechI& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
CMU::operator<(const ::CMU::SpeechI& l, const ::CMU::SpeechI& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
CMU::__patch__SpeechIPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::CMU::SpeechIPtr* p = static_cast< ::CMU::SpeechIPtr*>(__addr);
    assert(p);
    *p = ::CMU::SpeechIPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::CMU::SpeechI::ice_staticId(), v->ice_id());
    }
}

void
IceProxy::CMU::SpeechI::say(const ::std::string& message, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__SpeechI__say_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(message);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

const ::std::string&
IceProxy::CMU::SpeechI::ice_staticId()
{
    return __CMU__SpeechI_ids[0];
}

::IceProxy::Ice::Object*
IceProxy::CMU::SpeechI::__newInstance() const
{
    return new SpeechI;
}
