// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice-E version 1.3.0
// Generated from file `NavigationI.ice'

#include <NavigationI.h>
#include <IceE/LocalException.h>
#include <IceE/ObjectFactory.h>
#include <IceE/BasicStream.h>
#include <IceE/Iterator.h>

#ifndef ICEE_IGNORE_VERSION
#   if ICEE_INT_VERSION / 100 != 103
#       error IceE version mismatch!
#   endif
#   if ICEE_INT_VERSION % 100 < 0
#       error IceE patch level mismatch!
#   endif
#endif

static const ::std::string __CMU__NavigationI__whereAreYou_name = "whereAreYou";

static const ::std::string __CMU__NavigationI__goToOffices_name = "goToOffices";

static const ::std::string __CMU__NavigationI__goToOffice_name = "goToOffice";

static const ::std::string __CMU__NavigationI__escort_name = "escort";

static const ::std::string __CMU__NavigationI__cancelTask_name = "cancelTask";

::Ice::Object* IceInternal::upCast(::CMU::NavigationI* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::CMU::NavigationI* p) { return p; }

void
CMU::__read(::IceInternal::BasicStream* __is, ::CMU::NavigationIPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::CMU::NavigationI;
        v->__copyFrom(proxy);
    }
}

static const ::std::string __CMU__NavigationI_ids[2] =
{
    "::CMU::NavigationI",
    "::Ice::Object"
};

bool
CMU::NavigationI::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__CMU__NavigationI_ids, __CMU__NavigationI_ids + 2, _s);
}

::std::vector< ::std::string>
CMU::NavigationI::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__CMU__NavigationI_ids[0], &__CMU__NavigationI_ids[2]);
}

const ::std::string&
CMU::NavigationI::ice_id(const ::Ice::Current&) const
{
    return __CMU__NavigationI_ids[0];
}

const ::std::string&
CMU::NavigationI::ice_staticId()
{
    return __CMU__NavigationI_ids[0];
}

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::NavigationI::___whereAreYou(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::Float timeStamp;
    ::Ice::Float x;
    ::Ice::Float y;
    ::Ice::Float angle;
    ::Ice::Float conf;
    ::std::string map;
    whereAreYou(timeStamp, x, y, angle, conf, map, __current);
    __os->write(timeStamp);
    __os->write(x);
    __os->write(y);
    __os->write(angle);
    __os->write(conf);
    __os->write(map);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::NavigationI::___goToOffices(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::CMU::Offices officeList;
    __is->read(officeList);
    goToOffices(officeList, __current);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::NavigationI::___goToOffice(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string office;
    ::std::string startDate;
    ::std::string startH;
    ::std::string startM;
    ::std::string startP;
    ::std::string endDate;
    ::std::string endH;
    ::std::string endM;
    ::std::string endP;
    __is->read(office);
    __is->read(startDate);
    __is->read(startH);
    __is->read(startM);
    __is->read(startP);
    __is->read(endDate);
    __is->read(endH);
    __is->read(endM);
    __is->read(endP);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::NavigationI::___escort(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string room;
    ::std::string startDate;
    ::std::string startH;
    ::std::string startM;
    ::std::string startP;
    ::std::string endDate;
    ::std::string endH;
    ::std::string endM;
    ::std::string endP;
    __is->read(room);
    __is->read(startDate);
    __is->read(startH);
    __is->read(startM);
    __is->read(startP);
    __is->read(endDate);
    __is->read(endH);
    __is->read(endM);
    __is->read(endP);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
::Ice::DispatchStatus
CMU::NavigationI::___cancelTask(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    ::std::string taskId;
    __is->read(taskId);
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::Int __ret = cancelTask(taskId, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}
#endif // ICEE_PURE_CLIENT

#ifndef ICEE_PURE_CLIENT
static ::std::string __CMU__NavigationI_all[] =
{
    "cancelTask",
    "escort",
    "goToOffice",
    "goToOffices",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping",
    "whereAreYou"
};

::Ice::DispatchStatus
CMU::NavigationI::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__CMU__NavigationI_all, __CMU__NavigationI_all + 9, current.operation);
    if(r.first == r.second)
    {
        throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __CMU__NavigationI_all)
    {
        case 0:
        {
            return ___cancelTask(in, current);
        }
        case 1:
        {
            return ___escort(in, current);
        }
        case 2:
        {
            return ___goToOffice(in, current);
        }
        case 3:
        {
            return ___goToOffices(in, current);
        }
        case 4:
        {
            return ___ice_id(in, current);
        }
        case 5:
        {
            return ___ice_ids(in, current);
        }
        case 6:
        {
            return ___ice_isA(in, current);
        }
        case 7:
        {
            return ___ice_ping(in, current);
        }
        case 8:
        {
            return ___whereAreYou(in, current);
        }
    }

    assert(false);
    throw Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}
#endif // ICEE_PURE_CLIENT

void
CMU::NavigationI::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
    ::Ice::Object::__write(__os);
}

void
CMU::NavigationI::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
    ::Ice::Object::__read(__is, true);
}


bool
CMU::operator==(const ::CMU::NavigationI& l, const ::CMU::NavigationI& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
CMU::operator<(const ::CMU::NavigationI& l, const ::CMU::NavigationI& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

void 
CMU::__patch__NavigationIPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::CMU::NavigationIPtr* p = static_cast< ::CMU::NavigationIPtr*>(__addr);
    assert(p);
    *p = ::CMU::NavigationIPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::CMU::NavigationI::ice_staticId(), v->ice_id());
    }
}

void
IceProxy::CMU::NavigationI::whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__CMU__NavigationI__whereAreYou_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__NavigationI__whereAreYou_name, ::Ice::Normal, __ctx);
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(timeStamp);
                __is->read(x);
                __is->read(y);
                __is->read(angle);
                __is->read(conf);
                __is->read(map);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

void
IceProxy::CMU::NavigationI::goToOffices(const ::CMU::Offices& officeList, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__NavigationI__goToOffices_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                if(officeList.size() == 0)
                {
                    __os->writeSize(0);
                }
                else
                {
                    __os->write(&officeList[0], &officeList[0] + officeList.size());
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

::std::string
IceProxy::CMU::NavigationI::goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__CMU__NavigationI__goToOffice_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__NavigationI__goToOffice_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(office);
                __os->write(startDate);
                __os->write(startH);
                __os->write(startM);
                __os->write(startP);
                __os->write(endDate);
                __os->write(endH);
                __os->write(endM);
                __os->write(endP);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::std::string __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

::std::string
IceProxy::CMU::NavigationI::escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__CMU__NavigationI__escort_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__NavigationI__escort_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(room);
                __os->write(startDate);
                __os->write(startH);
                __os->write(startM);
                __os->write(startP);
                __os->write(endDate);
                __os->write(endH);
                __os->write(endM);
                __os->write(endP);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::std::string __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

::Ice::Int
IceProxy::CMU::NavigationI::cancelTask(const ::std::string& taskId, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::RequestHandlerPtr __handler;
        try
        {
            __checkTwowayOnly(__CMU__NavigationI__cancelTask_name);
            __handler = __getRequestHandler();
            ::IceInternal::Outgoing __outS(__handler.get(), _reference.get(), __CMU__NavigationI__cancelTask_name, ::Ice::Normal, __ctx);
            try
            {
                ::IceInternal::BasicStream* __os = __outS.os();
                __os->write(taskId);
            }
            catch(const ::Ice::LocalException& __ex)
            {
                __outS.abort(__ex);
            }
            bool __ok = __outS.invoke();
            try
            {
                if(!__ok)
                {
                    __outS.is()->throwUnknownUserException();
                }
                ::Ice::Int __ret;
                ::IceInternal::BasicStream* __is = __outS.is();
                __is->read(__ret);
                return __ret;
            }
            catch(const ::Ice::LocalException& __ex)
            {
                throw ::IceInternal::LocalExceptionWrapper(__ex, false);
            }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
            catch(...)
            {
                throw;
            }
#endif
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__handler, __ex);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__handler, __ex, __cnt);
        }
#if defined(_MSC_VER) && defined(_M_ARM) // ARM bug.
        catch(...)
        {
            throw;
        }
#endif
    }
}

const ::std::string&
IceProxy::CMU::NavigationI::ice_staticId()
{
    return __CMU__NavigationI_ids[0];
}

::IceProxy::Ice::Object*
IceProxy::CMU::NavigationI::__newInstance() const
{
    return new NavigationI;
}
