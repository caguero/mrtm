// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public interface _OntologyIOperations
{
    String[] getCurrentPath(Ice.Current __current);

    void setMyStartingPoint(String robotId, String office, Ice.Current __current);

    String[] getInterests(Ice.Current __current);

    String getCurrentContext(Ice.Current __current);
}
