// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public final class AgendaIPrxHelper extends Ice.ObjectPrxHelperBase implements AgendaIPrx
{
    public String
    officeOf(String personName)
    {
        return officeOf(personName, null, false);
    }

    public String
    officeOf(String personName, java.util.Map<String, String> __ctx)
    {
        return officeOf(personName, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private String
    officeOf(String personName, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("officeOf");
                __delBase = __getDelegate(false);
                _AgendaIDel __del = (_AgendaIDel)__delBase;
                return __del.officeOf(personName, __ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public static AgendaIPrx
    checkedCast(Ice.ObjectPrx __obj)
    {
        AgendaIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (AgendaIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::AgendaI"))
                {
                    AgendaIPrxHelper __h = new AgendaIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static AgendaIPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        AgendaIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (AgendaIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::AgendaI", __ctx))
                {
                    AgendaIPrxHelper __h = new AgendaIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static AgendaIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        AgendaIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::AgendaI"))
                {
                    AgendaIPrxHelper __h = new AgendaIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static AgendaIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        AgendaIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::AgendaI", __ctx))
                {
                    AgendaIPrxHelper __h = new AgendaIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static AgendaIPrx
    uncheckedCast(Ice.ObjectPrx __obj)
    {
        AgendaIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (AgendaIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                AgendaIPrxHelper __h = new AgendaIPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static AgendaIPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        AgendaIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            AgendaIPrxHelper __h = new AgendaIPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    protected Ice._ObjectDelM
    __createDelegateM()
    {
        return new _AgendaIDelM();
    }

    protected Ice._ObjectDelD
    __createDelegateD()
    {
        return new _AgendaIDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, AgendaIPrx v)
    {
        __os.writeProxy(v);
    }

    public static AgendaIPrx
    __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            AgendaIPrxHelper result = new AgendaIPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
