// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public final class OntologyIPrxHelper extends Ice.ObjectPrxHelperBase implements OntologyIPrx
{
    public String
    getCurrentContext()
    {
        return getCurrentContext(null, false);
    }

    public String
    getCurrentContext(java.util.Map<String, String> __ctx)
    {
        return getCurrentContext(__ctx, true);
    }

    @SuppressWarnings("unchecked")
    private String
    getCurrentContext(java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("getCurrentContext");
                __delBase = __getDelegate(false);
                _OntologyIDel __del = (_OntologyIDel)__delBase;
                return __del.getCurrentContext(__ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public String[]
    getCurrentPath()
    {
        return getCurrentPath(null, false);
    }

    public String[]
    getCurrentPath(java.util.Map<String, String> __ctx)
    {
        return getCurrentPath(__ctx, true);
    }

    @SuppressWarnings("unchecked")
    private String[]
    getCurrentPath(java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("getCurrentPath");
                __delBase = __getDelegate(false);
                _OntologyIDel __del = (_OntologyIDel)__delBase;
                return __del.getCurrentPath(__ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public String[]
    getInterests()
    {
        return getInterests(null, false);
    }

    public String[]
    getInterests(java.util.Map<String, String> __ctx)
    {
        return getInterests(__ctx, true);
    }

    @SuppressWarnings("unchecked")
    private String[]
    getInterests(java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("getInterests");
                __delBase = __getDelegate(false);
                _OntologyIDel __del = (_OntologyIDel)__delBase;
                return __del.getInterests(__ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public void
    setMyStartingPoint(String robotId, String office)
    {
        setMyStartingPoint(robotId, office, null, false);
    }

    public void
    setMyStartingPoint(String robotId, String office, java.util.Map<String, String> __ctx)
    {
        setMyStartingPoint(robotId, office, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private void
    setMyStartingPoint(String robotId, String office, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __delBase = __getDelegate(false);
                _OntologyIDel __del = (_OntologyIDel)__delBase;
                __del.setMyStartingPoint(robotId, office, __ctx);
                return;
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public static OntologyIPrx
    checkedCast(Ice.ObjectPrx __obj)
    {
        OntologyIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (OntologyIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::OntologyI"))
                {
                    OntologyIPrxHelper __h = new OntologyIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static OntologyIPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        OntologyIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (OntologyIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::OntologyI", __ctx))
                {
                    OntologyIPrxHelper __h = new OntologyIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static OntologyIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        OntologyIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::OntologyI"))
                {
                    OntologyIPrxHelper __h = new OntologyIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static OntologyIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        OntologyIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::OntologyI", __ctx))
                {
                    OntologyIPrxHelper __h = new OntologyIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static OntologyIPrx
    uncheckedCast(Ice.ObjectPrx __obj)
    {
        OntologyIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (OntologyIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                OntologyIPrxHelper __h = new OntologyIPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static OntologyIPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        OntologyIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            OntologyIPrxHelper __h = new OntologyIPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    protected Ice._ObjectDelM
    __createDelegateM()
    {
        return new _OntologyIDelM();
    }

    protected Ice._ObjectDelD
    __createDelegateD()
    {
        return new _OntologyIDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, OntologyIPrx v)
    {
        __os.writeProxy(v);
    }

    public static OntologyIPrx
    __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            OntologyIPrxHelper result = new OntologyIPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
