// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public abstract class _NavigationIDisp extends Ice.ObjectImpl implements NavigationI
{
    protected void
    ice_copyStateFrom(Ice.Object __obj)
        throws java.lang.CloneNotSupportedException
    {
        throw new java.lang.CloneNotSupportedException();
    }

    public static final String[] __ids =
    {
        "::CMU::NavigationI",
        "::Ice::Object"
    };

    public boolean
    ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean
    ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[]
    ice_ids()
    {
        return __ids;
    }

    public String[]
    ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String
    ice_id()
    {
        return __ids[0];
    }

    public String
    ice_id(Ice.Current __current)
    {
        return __ids[0];
    }

    public static String
    ice_staticId()
    {
        return __ids[0];
    }

    public final int
    cancelTask(String taskId)
    {
        return cancelTask(taskId, null);
    }

    public final String
    escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, null);
    }

    public final String
    goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, null);
    }

    public final void
    goToOffices(String[] officeList)
    {
        goToOffices(officeList, null);
    }

    public final void
    whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, null);
    }

    public static Ice.DispatchStatus
    ___whereAreYou(NavigationI __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        __inS.is().skipEmptyEncaps();
        Ice.FloatHolder timeStamp = new Ice.FloatHolder();
        Ice.FloatHolder x = new Ice.FloatHolder();
        Ice.FloatHolder y = new Ice.FloatHolder();
        Ice.FloatHolder angle = new Ice.FloatHolder();
        Ice.FloatHolder conf = new Ice.FloatHolder();
        Ice.StringHolder map = new Ice.StringHolder();
        IceInternal.BasicStream __os = __inS.os();
        __obj.whereAreYou(timeStamp, x, y, angle, conf, map, __current);
        __os.writeFloat(timeStamp.value);
        __os.writeFloat(x.value);
        __os.writeFloat(y.value);
        __os.writeFloat(angle.value);
        __os.writeFloat(conf.value);
        __os.writeString(map.value);
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus
    ___goToOffices(NavigationI __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        String[] officeList;
        officeList = OfficesHelper.read(__is);
        __is.endReadEncaps();
        __obj.goToOffices(officeList, __current);
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus
    ___goToOffice(NavigationI __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        String office;
        office = __is.readString();
        String startDate;
        startDate = __is.readString();
        String startH;
        startH = __is.readString();
        String startM;
        startM = __is.readString();
        String startP;
        startP = __is.readString();
        String endDate;
        endDate = __is.readString();
        String endH;
        endH = __is.readString();
        String endM;
        endM = __is.readString();
        String endP;
        endP = __is.readString();
        __is.endReadEncaps();
        IceInternal.BasicStream __os = __inS.os();
        String __ret = __obj.goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
        __os.writeString(__ret);
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus
    ___escort(NavigationI __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        String room;
        room = __is.readString();
        String startDate;
        startDate = __is.readString();
        String startH;
        startH = __is.readString();
        String startM;
        startM = __is.readString();
        String startP;
        startP = __is.readString();
        String endDate;
        endDate = __is.readString();
        String endH;
        endH = __is.readString();
        String endM;
        endM = __is.readString();
        String endP;
        endP = __is.readString();
        __is.endReadEncaps();
        IceInternal.BasicStream __os = __inS.os();
        String __ret = __obj.escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
        __os.writeString(__ret);
        return Ice.DispatchStatus.DispatchOK;
    }

    public static Ice.DispatchStatus
    ___cancelTask(NavigationI __obj, IceInternal.Incoming __inS, Ice.Current __current)
    {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        __is.startReadEncaps();
        String taskId;
        taskId = __is.readString();
        __is.endReadEncaps();
        IceInternal.BasicStream __os = __inS.os();
        int __ret = __obj.cancelTask(taskId, __current);
        __os.writeInt(__ret);
        return Ice.DispatchStatus.DispatchOK;
    }

    private final static String[] __all =
    {
        "cancelTask",
        "escort",
        "goToOffice",
        "goToOffices",
        "ice_id",
        "ice_ids",
        "ice_isA",
        "ice_ping",
        "whereAreYou"
    };

    public Ice.DispatchStatus
    __dispatch(IceInternal.Incoming in, Ice.Current __current)
    {
        int pos = java.util.Arrays.binarySearch(__all, __current.operation);
        if(pos < 0)
        {
            throw new Ice.OperationNotExistException(__current.id, __current.facet, __current.operation);
        }

        switch(pos)
        {
            case 0:
            {
                return ___cancelTask(this, in, __current);
            }
            case 1:
            {
                return ___escort(this, in, __current);
            }
            case 2:
            {
                return ___goToOffice(this, in, __current);
            }
            case 3:
            {
                return ___goToOffices(this, in, __current);
            }
            case 4:
            {
                return ___ice_id(this, in, __current);
            }
            case 5:
            {
                return ___ice_ids(this, in, __current);
            }
            case 6:
            {
                return ___ice_isA(this, in, __current);
            }
            case 7:
            {
                return ___ice_ping(this, in, __current);
            }
            case 8:
            {
                return ___whereAreYou(this, in, __current);
            }
        }

        assert(false);
        throw new Ice.OperationNotExistException(__current.id, __current.facet, __current.operation);
    }

    public void
    __write(IceInternal.BasicStream __os)
    {
        __os.writeTypeId(ice_staticId());
        __os.startWriteSlice();
        __os.endWriteSlice();
        super.__write(__os);
    }

    public void
    __read(IceInternal.BasicStream __is, boolean __rid)
    {
        if(__rid)
        {
            __is.readTypeId();
        }
        __is.startReadSlice();
        __is.endReadSlice();
        super.__read(__is, true);
    }

    public void
    __write(Ice.OutputStream __outS)
    {
        Ice.MarshalException ex = new Ice.MarshalException();
        ex.reason = "type CMU::NavigationI was not generated with stream support";
        throw ex;
    }

    public void
    __read(Ice.InputStream __inS, boolean __rid)
    {
        Ice.MarshalException ex = new Ice.MarshalException();
        ex.reason = "type CMU::NavigationI was not generated with stream support";
        throw ex;
    }
}
