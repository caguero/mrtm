// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public final class NavigationIPrxHelper extends Ice.ObjectPrxHelperBase implements NavigationIPrx
{
    public int
    cancelTask(String taskId)
    {
        return cancelTask(taskId, null, false);
    }

    public int
    cancelTask(String taskId, java.util.Map<String, String> __ctx)
    {
        return cancelTask(taskId, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private int
    cancelTask(String taskId, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("cancelTask");
                __delBase = __getDelegate(false);
                _NavigationIDel __del = (_NavigationIDel)__delBase;
                return __del.cancelTask(taskId, __ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public String
    escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, null, false);
    }

    public String
    escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP, java.util.Map<String, String> __ctx)
    {
        return escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private String
    escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("escort");
                __delBase = __getDelegate(false);
                _NavigationIDel __del = (_NavigationIDel)__delBase;
                return __del.escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, __ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public String
    goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, null, false);
    }

    public String
    goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP, java.util.Map<String, String> __ctx)
    {
        return goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private String
    goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("goToOffice");
                __delBase = __getDelegate(false);
                _NavigationIDel __del = (_NavigationIDel)__delBase;
                return __del.goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, __ctx);
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public void
    goToOffices(String[] officeList)
    {
        goToOffices(officeList, null, false);
    }

    public void
    goToOffices(String[] officeList, java.util.Map<String, String> __ctx)
    {
        goToOffices(officeList, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private void
    goToOffices(String[] officeList, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __delBase = __getDelegate(false);
                _NavigationIDel __del = (_NavigationIDel)__delBase;
                __del.goToOffices(officeList, __ctx);
                return;
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public void
    whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, null, false);
    }

    public void
    whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map, java.util.Map<String, String> __ctx)
    {
        whereAreYou(timeStamp, x, y, angle, conf, map, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private void
    whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __checkTwowayOnly("whereAreYou");
                __delBase = __getDelegate(false);
                _NavigationIDel __del = (_NavigationIDel)__delBase;
                __del.whereAreYou(timeStamp, x, y, angle, conf, map, __ctx);
                return;
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public static NavigationIPrx
    checkedCast(Ice.ObjectPrx __obj)
    {
        NavigationIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (NavigationIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::NavigationI"))
                {
                    NavigationIPrxHelper __h = new NavigationIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static NavigationIPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        NavigationIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (NavigationIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::NavigationI", __ctx))
                {
                    NavigationIPrxHelper __h = new NavigationIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static NavigationIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        NavigationIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::NavigationI"))
                {
                    NavigationIPrxHelper __h = new NavigationIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static NavigationIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        NavigationIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::NavigationI", __ctx))
                {
                    NavigationIPrxHelper __h = new NavigationIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static NavigationIPrx
    uncheckedCast(Ice.ObjectPrx __obj)
    {
        NavigationIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (NavigationIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                NavigationIPrxHelper __h = new NavigationIPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static NavigationIPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        NavigationIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            NavigationIPrxHelper __h = new NavigationIPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    protected Ice._ObjectDelM
    __createDelegateM()
    {
        return new _NavigationIDelM();
    }

    protected Ice._ObjectDelD
    __createDelegateD()
    {
        return new _NavigationIDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, NavigationIPrx v)
    {
        __os.writeProxy(v);
    }

    public static NavigationIPrx
    __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            NavigationIPrxHelper result = new NavigationIPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
