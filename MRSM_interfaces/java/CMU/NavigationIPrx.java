// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public interface NavigationIPrx extends Ice.ObjectPrx
{
    public void whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map);
    public void whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map, java.util.Map<String, String> __ctx);

    public void goToOffices(String[] officeList);
    public void goToOffices(String[] officeList, java.util.Map<String, String> __ctx);

    public String goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP);
    public String goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP, java.util.Map<String, String> __ctx);

    public String escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP);
    public String escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP, java.util.Map<String, String> __ctx);

    public int cancelTask(String taskId);
    public int cancelTask(String taskId, java.util.Map<String, String> __ctx);
}
