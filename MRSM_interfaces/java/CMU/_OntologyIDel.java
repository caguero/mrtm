// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public interface _OntologyIDel extends Ice._ObjectDel
{
    String[] getCurrentPath(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    void setMyStartingPoint(String robotId, String office, java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    String[] getInterests(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;

    String getCurrentContext(java.util.Map<String, String> __ctx)
        throws IceInternal.LocalExceptionWrapper;
}
