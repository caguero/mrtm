// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public interface _NavigationIOperationsNC
{
    void whereAreYou(Ice.FloatHolder timeStamp, Ice.FloatHolder x, Ice.FloatHolder y, Ice.FloatHolder angle, Ice.FloatHolder conf, Ice.StringHolder map);

    void goToOffices(String[] officeList);

    String goToOffice(String office, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP);

    String escort(String room, String startDate, String startH, String startM, String startP, String endDate, String endH, String endM, String endP);

    int cancelTask(String taskId);
}
