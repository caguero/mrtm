// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1

package CMU;

public final class SpeechIPrxHelper extends Ice.ObjectPrxHelperBase implements SpeechIPrx
{
    public void
    say(String message)
    {
        say(message, null, false);
    }

    public void
    say(String message, java.util.Map<String, String> __ctx)
    {
        say(message, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private void
    say(String message, java.util.Map<String, String> __ctx, boolean __explicitCtx)
    {
        if(__explicitCtx && __ctx == null)
        {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while(true)
        {
            Ice._ObjectDel __delBase = null;
            try
            {
                __delBase = __getDelegate(false);
                _SpeechIDel __del = (_SpeechIDel)__delBase;
                __del.say(message, __ctx);
                return;
            }
            catch(IceInternal.LocalExceptionWrapper __ex)
            {
                __handleExceptionWrapper(__delBase, __ex, null);
            }
            catch(Ice.LocalException __ex)
            {
                __cnt = __handleException(__delBase, __ex, null, __cnt);
            }
        }
    }

    public static SpeechIPrx
    checkedCast(Ice.ObjectPrx __obj)
    {
        SpeechIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (SpeechIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::SpeechI"))
                {
                    SpeechIPrxHelper __h = new SpeechIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static SpeechIPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        SpeechIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (SpeechIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                if(__obj.ice_isA("::CMU::SpeechI", __ctx))
                {
                    SpeechIPrxHelper __h = new SpeechIPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static SpeechIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        SpeechIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::SpeechI"))
                {
                    SpeechIPrxHelper __h = new SpeechIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static SpeechIPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        SpeechIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA("::CMU::SpeechI", __ctx))
                {
                    SpeechIPrxHelper __h = new SpeechIPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static SpeechIPrx
    uncheckedCast(Ice.ObjectPrx __obj)
    {
        SpeechIPrx __d = null;
        if(__obj != null)
        {
            try
            {
                __d = (SpeechIPrx)__obj;
            }
            catch(ClassCastException ex)
            {
                SpeechIPrxHelper __h = new SpeechIPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static SpeechIPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        SpeechIPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            SpeechIPrxHelper __h = new SpeechIPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    protected Ice._ObjectDelM
    __createDelegateM()
    {
        return new _SpeechIDelM();
    }

    protected Ice._ObjectDelD
    __createDelegateD()
    {
        return new _SpeechIDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, SpeechIPrx v)
    {
        __os.writeProxy(v);
    }

    public static SpeechIPrx
    __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            SpeechIPrxHelper result = new SpeechIPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
