#!/bin/sh

#Name: buildInterfaces.sh
#@Author: Carlos Agüero (caguero@gsyc.es)
#Description: Script that generates cpp and java stubs from the slice interfaces. 'slice2cppe' is used to generate the nao cpp versions (Nao uses Icee instead of regular Ice).

slice2cpp --output-dir ../cpp/ ../slice/*.ice
slice2java --output-dir ../java/ ../slice/*.ice
/home/carlos/nao/player/bin/slice2cppe --output-dir ../cpp_nao/ ../slice/*.ice
