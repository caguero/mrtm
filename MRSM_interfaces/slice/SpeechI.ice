/*
 * Name: SpeechI.ice
 * @Author: Carlos Agüero (caguero@gsyc.es)
 * Description: Slice interface for any speech operation.
 */

#ifndef SpeechI_ICE
#define SpeechI_ICE

module CMU {
  
  /** 
  * Interface to the speech behavior.
  */
  interface SpeechI
  {
    /** 
     * Says a message (using voice)
     */
    void say( string message );  
  };
};

#endif //SpeechI_ICE
