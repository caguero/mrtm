/*
 * Name: OntologyI.ice
 * @Author: Carlos Agüero (caguero@gsyc.es)
 * Description: Slice interface for any ontology operation.
 */

#ifndef OntologyI_ICE
#define OntologyI_ICE

#include "Common.ice"

module CMU {
  
  /** 
  * Interface to the Ontology behavior.
  */
  interface OntologyI
  {
	Offices getCurrentPath();
	void setMyStartingPoint(string robotId, string office);
	Topics getInterests();
	string getCurrentContext();
  };
};

#endif //OntologyI_ICE
