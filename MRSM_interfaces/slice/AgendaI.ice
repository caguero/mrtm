/*
 * Name: AgendaI.ice
 * @Author: Carlos Agüero (caguero@gsyc.es)
 * Description: Slice interface for any agenda operation.
 */

#ifndef AgendaI_ICE
#define AgendaI_ICE

module CMU {
  
  /** 
  * Interface to the agenda behavior.
  */
  interface AgendaI
  {
    /** 
     * Returns the office number associated to a given person
     */
    string officeOf( string personName );
  };
};

#endif //AgendaI_ICE
