/*
 * Name: Common.ice
 * @Author: Carlos Agüero (caguero@gsyc.es)
 * Description: Slice interface for common datatypes and symbols.
 */

#ifndef COMMON_ICE
#define COMMON_ICE

module CMU{
	sequence<string> Offices;
	sequence<string> Topics;
};

#endif /*COMMON_ICE*/
