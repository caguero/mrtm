/*
 * Name: NavigationI.ice
 * @Author: Carlos Agüero (caguero@gsyc.es)
 * Description: Slice interface for any navigation operation.
 */


#ifndef NavigationI_ICE
#define NavigationI_ICE

#include "Common.ice"

module CMU {
       
  /**
  * Interface to the navigation behavior
  */ 
  interface NavigationI
  {  	
  	/**
  	* Goes to a specific location
  	*/
  	//void goToXY( string floor, float x, float y, float theta );
  	
  	void whereAreYou(out float timeStamp, out float x, out float y,
  	 out float angle, out float conf, out string map);
  	void goToOffices(Offices officeList);
  	string goToOffice(string office, string startDate, string startH, string startM,
  	       string startP, string endDate, string endH, string endM, string endP);
  	string escort( string room, string startDate, string startH, string startM,
  	       string startP, string endDate, string endH, string endM, string endP );
  	int cancelTask( string taskId );
  };
};

#endif //NavigationI_ICE
