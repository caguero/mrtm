// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1
// Generated from file `OntologyI.ice'

#include <OntologyI.h>
#include <Ice/LocalException.h>
#include <Ice/ObjectFactory.h>
#include <Ice/BasicStream.h>
#include <IceUtil/Iterator.h>
#include <IceUtil/ScopedArray.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 303
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 1
#       error Ice patch level mismatch!
#   endif
#endif

static const ::std::string __CMU__OntologyI__getCurrentPath_name = "getCurrentPath";

static const ::std::string __CMU__OntologyI__setMyStartingPoint_name = "setMyStartingPoint";

static const ::std::string __CMU__OntologyI__getInterests_name = "getInterests";

static const ::std::string __CMU__OntologyI__getCurrentContext_name = "getCurrentContext";

::Ice::Object* IceInternal::upCast(::CMU::OntologyI* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::CMU::OntologyI* p) { return p; }

void
CMU::__read(::IceInternal::BasicStream* __is, ::CMU::OntologyIPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::CMU::OntologyI;
        v->__copyFrom(proxy);
    }
}

::CMU::Offices
IceProxy::CMU::OntologyI::getCurrentPath(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__OntologyI__getCurrentPath_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::OntologyI* __del = dynamic_cast< ::IceDelegate::CMU::OntologyI*>(__delBase.get());
            return __del->getCurrentPath(__ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

void
IceProxy::CMU::OntologyI::setMyStartingPoint(const ::std::string& robotId, const ::std::string& office, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::OntologyI* __del = dynamic_cast< ::IceDelegate::CMU::OntologyI*>(__delBase.get());
            __del->setMyStartingPoint(robotId, office, __ctx);
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

::CMU::Topics
IceProxy::CMU::OntologyI::getInterests(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__OntologyI__getInterests_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::OntologyI* __del = dynamic_cast< ::IceDelegate::CMU::OntologyI*>(__delBase.get());
            return __del->getInterests(__ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

::std::string
IceProxy::CMU::OntologyI::getCurrentContext(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__OntologyI__getCurrentContext_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::OntologyI* __del = dynamic_cast< ::IceDelegate::CMU::OntologyI*>(__delBase.get());
            return __del->getCurrentContext(__ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

const ::std::string&
IceProxy::CMU::OntologyI::ice_staticId()
{
    return ::CMU::OntologyI::ice_staticId();
}

::IceInternal::Handle< ::IceDelegateM::Ice::Object>
IceProxy::CMU::OntologyI::__createDelegateM()
{
    return ::IceInternal::Handle< ::IceDelegateM::Ice::Object>(new ::IceDelegateM::CMU::OntologyI);
}

::IceInternal::Handle< ::IceDelegateD::Ice::Object>
IceProxy::CMU::OntologyI::__createDelegateD()
{
    return ::IceInternal::Handle< ::IceDelegateD::Ice::Object>(new ::IceDelegateD::CMU::OntologyI);
}

::IceProxy::Ice::Object*
IceProxy::CMU::OntologyI::__newInstance() const
{
    return new OntologyI;
}

::CMU::Offices
IceDelegateM::CMU::OntologyI::getCurrentPath(const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__OntologyI__getCurrentPath_name, ::Ice::Normal, __context);
    bool __ok = __og.invoke();
    ::CMU::Offices __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(__ret);
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

void
IceDelegateM::CMU::OntologyI::setMyStartingPoint(const ::std::string& robotId, const ::std::string& office, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__OntologyI__setMyStartingPoint_name, ::Ice::Normal, __context);
    try
    {
        ::IceInternal::BasicStream* __os = __og.os();
        __os->write(robotId);
        __os->write(office);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    bool __ok = __og.invoke();
    if(!__og.is()->b.empty())
    {
        try
        {
            if(!__ok)
            {
                try
                {
                    __og.throwUserException();
                }
                catch(const ::Ice::UserException& __ex)
                {
                    ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                    throw __uue;
                }
            }
            __og.is()->skipEmptyEncaps();
        }
        catch(const ::Ice::LocalException& __ex)
        {
            throw ::IceInternal::LocalExceptionWrapper(__ex, false);
        }
    }
}

::CMU::Topics
IceDelegateM::CMU::OntologyI::getInterests(const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__OntologyI__getInterests_name, ::Ice::Normal, __context);
    bool __ok = __og.invoke();
    ::CMU::Topics __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(__ret);
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

::std::string
IceDelegateM::CMU::OntologyI::getCurrentContext(const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__OntologyI__getCurrentContext_name, ::Ice::Normal, __context);
    bool __ok = __og.invoke();
    ::std::string __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(__ret);
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

::CMU::Offices
IceDelegateD::CMU::OntologyI::getCurrentPath(const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::CMU::Offices& __result, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::OntologyI* servant = dynamic_cast< ::CMU::OntologyI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->getCurrentPath(_current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::CMU::Offices& _result;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__OntologyI__getCurrentPath_name, ::Ice::Normal, __context);
    ::CMU::Offices __result;
    try
    {
        _DirectI __direct(__result, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

void
IceDelegateD::CMU::OntologyI::setMyStartingPoint(const ::std::string& robotId, const ::std::string& office, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(const ::std::string& robotId, const ::std::string& office, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _m_robotId(robotId),
            _m_office(office)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::OntologyI* servant = dynamic_cast< ::CMU::OntologyI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            servant->setMyStartingPoint(_m_robotId, _m_office, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        const ::std::string& _m_robotId;
        const ::std::string& _m_office;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__OntologyI__setMyStartingPoint_name, ::Ice::Normal, __context);
    try
    {
        _DirectI __direct(robotId, office, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
}

::CMU::Topics
IceDelegateD::CMU::OntologyI::getInterests(const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::CMU::Topics& __result, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::OntologyI* servant = dynamic_cast< ::CMU::OntologyI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->getInterests(_current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::CMU::Topics& _result;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__OntologyI__getInterests_name, ::Ice::Normal, __context);
    ::CMU::Topics __result;
    try
    {
        _DirectI __direct(__result, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

::std::string
IceDelegateD::CMU::OntologyI::getCurrentContext(const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::std::string& __result, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::OntologyI* servant = dynamic_cast< ::CMU::OntologyI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->getCurrentContext(_current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::std::string& _result;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__OntologyI__getCurrentContext_name, ::Ice::Normal, __context);
    ::std::string __result;
    try
    {
        _DirectI __direct(__result, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

::Ice::ObjectPtr
CMU::OntologyI::ice_clone() const
{
    throw ::Ice::CloneNotImplementedException(__FILE__, __LINE__);
    return 0; // to avoid a warning with some compilers
}

static const ::std::string __CMU__OntologyI_ids[2] =
{
    "::CMU::OntologyI",
    "::Ice::Object"
};

bool
CMU::OntologyI::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__CMU__OntologyI_ids, __CMU__OntologyI_ids + 2, _s);
}

::std::vector< ::std::string>
CMU::OntologyI::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__CMU__OntologyI_ids[0], &__CMU__OntologyI_ids[2]);
}

const ::std::string&
CMU::OntologyI::ice_id(const ::Ice::Current&) const
{
    return __CMU__OntologyI_ids[0];
}

const ::std::string&
CMU::OntologyI::ice_staticId()
{
    return __CMU__OntologyI_ids[0];
}

::Ice::DispatchStatus
CMU::OntologyI::___getCurrentPath(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    __inS.is()->skipEmptyEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::CMU::Offices __ret = getCurrentPath(__current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::OntologyI::___setMyStartingPoint(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    __is->startReadEncaps();
    ::std::string robotId;
    ::std::string office;
    __is->read(robotId);
    __is->read(office);
    __is->endReadEncaps();
    setMyStartingPoint(robotId, office, __current);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::OntologyI::___getInterests(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    __inS.is()->skipEmptyEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::CMU::Topics __ret = getInterests(__current);
    if(__ret.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&__ret[0], &__ret[0] + __ret.size());
    }
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::OntologyI::___getCurrentContext(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    __inS.is()->skipEmptyEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = getCurrentContext(__current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}

static ::std::string __CMU__OntologyI_all[] =
{
    "getCurrentContext",
    "getCurrentPath",
    "getInterests",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping",
    "setMyStartingPoint"
};

::Ice::DispatchStatus
CMU::OntologyI::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__CMU__OntologyI_all, __CMU__OntologyI_all + 8, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __CMU__OntologyI_all)
    {
        case 0:
        {
            return ___getCurrentContext(in, current);
        }
        case 1:
        {
            return ___getCurrentPath(in, current);
        }
        case 2:
        {
            return ___getInterests(in, current);
        }
        case 3:
        {
            return ___ice_id(in, current);
        }
        case 4:
        {
            return ___ice_ids(in, current);
        }
        case 5:
        {
            return ___ice_isA(in, current);
        }
        case 6:
        {
            return ___ice_ping(in, current);
        }
        case 7:
        {
            return ___setMyStartingPoint(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
CMU::OntologyI::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__write(__os);
#else
    ::Ice::Object::__write(__os);
#endif
}

void
CMU::OntologyI::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__read(__is, true);
#else
    ::Ice::Object::__read(__is, true);
#endif
}

void
CMU::OntologyI::__write(const ::Ice::OutputStreamPtr&) const
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type CMU::OntologyI was not generated with stream support";
    throw ex;
}

void
CMU::OntologyI::__read(const ::Ice::InputStreamPtr&, bool)
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type CMU::OntologyI was not generated with stream support";
    throw ex;
}

void 
CMU::__patch__OntologyIPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::CMU::OntologyIPtr* p = static_cast< ::CMU::OntologyIPtr*>(__addr);
    assert(p);
    *p = ::CMU::OntologyIPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::CMU::OntologyI::ice_staticId(), v->ice_id());
    }
}

bool
CMU::operator==(const ::CMU::OntologyI& l, const ::CMU::OntologyI& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
CMU::operator<(const ::CMU::OntologyI& l, const ::CMU::OntologyI& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}
