// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1
// Generated from file `NavigationI.ice'

#include <NavigationI.h>
#include <Ice/LocalException.h>
#include <Ice/ObjectFactory.h>
#include <Ice/BasicStream.h>
#include <IceUtil/Iterator.h>
#include <IceUtil/ScopedArray.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 303
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 1
#       error Ice patch level mismatch!
#   endif
#endif

static const ::std::string __CMU__NavigationI__whereAreYou_name = "whereAreYou";

static const ::std::string __CMU__NavigationI__goToOffices_name = "goToOffices";

static const ::std::string __CMU__NavigationI__goToOffice_name = "goToOffice";

static const ::std::string __CMU__NavigationI__escort_name = "escort";

static const ::std::string __CMU__NavigationI__cancelTask_name = "cancelTask";

::Ice::Object* IceInternal::upCast(::CMU::NavigationI* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::CMU::NavigationI* p) { return p; }

void
CMU::__read(::IceInternal::BasicStream* __is, ::CMU::NavigationIPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::CMU::NavigationI;
        v->__copyFrom(proxy);
    }
}

void
IceProxy::CMU::NavigationI::whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__NavigationI__whereAreYou_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::NavigationI* __del = dynamic_cast< ::IceDelegate::CMU::NavigationI*>(__delBase.get());
            __del->whereAreYou(timeStamp, x, y, angle, conf, map, __ctx);
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

void
IceProxy::CMU::NavigationI::goToOffices(const ::CMU::Offices& officeList, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::NavigationI* __del = dynamic_cast< ::IceDelegate::CMU::NavigationI*>(__delBase.get());
            __del->goToOffices(officeList, __ctx);
            return;
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

::std::string
IceProxy::CMU::NavigationI::goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__NavigationI__goToOffice_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::NavigationI* __del = dynamic_cast< ::IceDelegate::CMU::NavigationI*>(__delBase.get());
            return __del->goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, __ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

::std::string
IceProxy::CMU::NavigationI::escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__NavigationI__escort_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::NavigationI* __del = dynamic_cast< ::IceDelegate::CMU::NavigationI*>(__delBase.get());
            return __del->escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, __ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

::Ice::Int
IceProxy::CMU::NavigationI::cancelTask(const ::std::string& taskId, const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__CMU__NavigationI__cancelTask_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::CMU::NavigationI* __del = dynamic_cast< ::IceDelegate::CMU::NavigationI*>(__delBase.get());
            return __del->cancelTask(taskId, __ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapper(__delBase, __ex, 0);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

const ::std::string&
IceProxy::CMU::NavigationI::ice_staticId()
{
    return ::CMU::NavigationI::ice_staticId();
}

::IceInternal::Handle< ::IceDelegateM::Ice::Object>
IceProxy::CMU::NavigationI::__createDelegateM()
{
    return ::IceInternal::Handle< ::IceDelegateM::Ice::Object>(new ::IceDelegateM::CMU::NavigationI);
}

::IceInternal::Handle< ::IceDelegateD::Ice::Object>
IceProxy::CMU::NavigationI::__createDelegateD()
{
    return ::IceInternal::Handle< ::IceDelegateD::Ice::Object>(new ::IceDelegateD::CMU::NavigationI);
}

::IceProxy::Ice::Object*
IceProxy::CMU::NavigationI::__newInstance() const
{
    return new NavigationI;
}

void
IceDelegateM::CMU::NavigationI::whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__NavigationI__whereAreYou_name, ::Ice::Normal, __context);
    bool __ok = __og.invoke();
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(timeStamp);
        __is->read(x);
        __is->read(y);
        __is->read(angle);
        __is->read(conf);
        __is->read(map);
        __is->endReadEncaps();
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

void
IceDelegateM::CMU::NavigationI::goToOffices(const ::CMU::Offices& officeList, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__NavigationI__goToOffices_name, ::Ice::Normal, __context);
    try
    {
        ::IceInternal::BasicStream* __os = __og.os();
        if(officeList.size() == 0)
        {
            __os->writeSize(0);
        }
        else
        {
            __os->write(&officeList[0], &officeList[0] + officeList.size());
        }
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    bool __ok = __og.invoke();
    if(!__og.is()->b.empty())
    {
        try
        {
            if(!__ok)
            {
                try
                {
                    __og.throwUserException();
                }
                catch(const ::Ice::UserException& __ex)
                {
                    ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                    throw __uue;
                }
            }
            __og.is()->skipEmptyEncaps();
        }
        catch(const ::Ice::LocalException& __ex)
        {
            throw ::IceInternal::LocalExceptionWrapper(__ex, false);
        }
    }
}

::std::string
IceDelegateM::CMU::NavigationI::goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__NavigationI__goToOffice_name, ::Ice::Normal, __context);
    try
    {
        ::IceInternal::BasicStream* __os = __og.os();
        __os->write(office);
        __os->write(startDate);
        __os->write(startH);
        __os->write(startM);
        __os->write(startP);
        __os->write(endDate);
        __os->write(endH);
        __os->write(endM);
        __os->write(endP);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    bool __ok = __og.invoke();
    ::std::string __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(__ret);
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

::std::string
IceDelegateM::CMU::NavigationI::escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__NavigationI__escort_name, ::Ice::Normal, __context);
    try
    {
        ::IceInternal::BasicStream* __os = __og.os();
        __os->write(room);
        __os->write(startDate);
        __os->write(startH);
        __os->write(startM);
        __os->write(startP);
        __os->write(endDate);
        __os->write(endH);
        __os->write(endM);
        __os->write(endP);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    bool __ok = __og.invoke();
    ::std::string __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(__ret);
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

::Ice::Int
IceDelegateM::CMU::NavigationI::cancelTask(const ::std::string& taskId, const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __CMU__NavigationI__cancelTask_name, ::Ice::Normal, __context);
    try
    {
        ::IceInternal::BasicStream* __os = __og.os();
        __os->write(taskId);
    }
    catch(const ::Ice::LocalException& __ex)
    {
        __og.abort(__ex);
    }
    bool __ok = __og.invoke();
    ::Ice::Int __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(__ret);
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

void
IceDelegateD::CMU::NavigationI::whereAreYou(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::Ice::Float& timeStamp, ::Ice::Float& x, ::Ice::Float& y, ::Ice::Float& angle, ::Ice::Float& conf, ::std::string& map, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _m_timeStamp(timeStamp),
            _m_x(x),
            _m_y(y),
            _m_angle(angle),
            _m_conf(conf),
            _m_map(map)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::NavigationI* servant = dynamic_cast< ::CMU::NavigationI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            servant->whereAreYou(_m_timeStamp, _m_x, _m_y, _m_angle, _m_conf, _m_map, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::Ice::Float& _m_timeStamp;
        ::Ice::Float& _m_x;
        ::Ice::Float& _m_y;
        ::Ice::Float& _m_angle;
        ::Ice::Float& _m_conf;
        ::std::string& _m_map;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__NavigationI__whereAreYou_name, ::Ice::Normal, __context);
    try
    {
        _DirectI __direct(timeStamp, x, y, angle, conf, map, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
}

void
IceDelegateD::CMU::NavigationI::goToOffices(const ::CMU::Offices& officeList, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(const ::CMU::Offices& officeList, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _m_officeList(officeList)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::NavigationI* servant = dynamic_cast< ::CMU::NavigationI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            servant->goToOffices(_m_officeList, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        const ::CMU::Offices& _m_officeList;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__NavigationI__goToOffices_name, ::Ice::Normal, __context);
    try
    {
        _DirectI __direct(officeList, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
}

::std::string
IceDelegateD::CMU::NavigationI::goToOffice(const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::std::string& __result, const ::std::string& office, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result),
            _m_office(office),
            _m_startDate(startDate),
            _m_startH(startH),
            _m_startM(startM),
            _m_startP(startP),
            _m_endDate(endDate),
            _m_endH(endH),
            _m_endM(endM),
            _m_endP(endP)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::NavigationI* servant = dynamic_cast< ::CMU::NavigationI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->goToOffice(_m_office, _m_startDate, _m_startH, _m_startM, _m_startP, _m_endDate, _m_endH, _m_endM, _m_endP, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::std::string& _result;
        const ::std::string& _m_office;
        const ::std::string& _m_startDate;
        const ::std::string& _m_startH;
        const ::std::string& _m_startM;
        const ::std::string& _m_startP;
        const ::std::string& _m_endDate;
        const ::std::string& _m_endH;
        const ::std::string& _m_endM;
        const ::std::string& _m_endP;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__NavigationI__goToOffice_name, ::Ice::Normal, __context);
    ::std::string __result;
    try
    {
        _DirectI __direct(__result, office, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

::std::string
IceDelegateD::CMU::NavigationI::escort(const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::std::string& __result, const ::std::string& room, const ::std::string& startDate, const ::std::string& startH, const ::std::string& startM, const ::std::string& startP, const ::std::string& endDate, const ::std::string& endH, const ::std::string& endM, const ::std::string& endP, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result),
            _m_room(room),
            _m_startDate(startDate),
            _m_startH(startH),
            _m_startM(startM),
            _m_startP(startP),
            _m_endDate(endDate),
            _m_endH(endH),
            _m_endM(endM),
            _m_endP(endP)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::NavigationI* servant = dynamic_cast< ::CMU::NavigationI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->escort(_m_room, _m_startDate, _m_startH, _m_startM, _m_startP, _m_endDate, _m_endH, _m_endM, _m_endP, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::std::string& _result;
        const ::std::string& _m_room;
        const ::std::string& _m_startDate;
        const ::std::string& _m_startH;
        const ::std::string& _m_startM;
        const ::std::string& _m_startP;
        const ::std::string& _m_endDate;
        const ::std::string& _m_endH;
        const ::std::string& _m_endM;
        const ::std::string& _m_endP;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__NavigationI__escort_name, ::Ice::Normal, __context);
    ::std::string __result;
    try
    {
        _DirectI __direct(__result, room, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

::Ice::Int
IceDelegateD::CMU::NavigationI::cancelTask(const ::std::string& taskId, const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::Ice::Int& __result, const ::std::string& taskId, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result),
            _m_taskId(taskId)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::CMU::NavigationI* servant = dynamic_cast< ::CMU::NavigationI*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->cancelTask(_m_taskId, _current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::Ice::Int& _result;
        const ::std::string& _m_taskId;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __CMU__NavigationI__cancelTask_name, ::Ice::Normal, __context);
    ::Ice::Int __result;
    try
    {
        _DirectI __direct(__result, taskId, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

::Ice::ObjectPtr
CMU::NavigationI::ice_clone() const
{
    throw ::Ice::CloneNotImplementedException(__FILE__, __LINE__);
    return 0; // to avoid a warning with some compilers
}

static const ::std::string __CMU__NavigationI_ids[2] =
{
    "::CMU::NavigationI",
    "::Ice::Object"
};

bool
CMU::NavigationI::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__CMU__NavigationI_ids, __CMU__NavigationI_ids + 2, _s);
}

::std::vector< ::std::string>
CMU::NavigationI::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__CMU__NavigationI_ids[0], &__CMU__NavigationI_ids[2]);
}

const ::std::string&
CMU::NavigationI::ice_id(const ::Ice::Current&) const
{
    return __CMU__NavigationI_ids[0];
}

const ::std::string&
CMU::NavigationI::ice_staticId()
{
    return __CMU__NavigationI_ids[0];
}

::Ice::DispatchStatus
CMU::NavigationI::___whereAreYou(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    __inS.is()->skipEmptyEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::Float timeStamp;
    ::Ice::Float x;
    ::Ice::Float y;
    ::Ice::Float angle;
    ::Ice::Float conf;
    ::std::string map;
    whereAreYou(timeStamp, x, y, angle, conf, map, __current);
    __os->write(timeStamp);
    __os->write(x);
    __os->write(y);
    __os->write(angle);
    __os->write(conf);
    __os->write(map);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::NavigationI::___goToOffices(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    __is->startReadEncaps();
    ::CMU::Offices officeList;
    __is->read(officeList);
    __is->endReadEncaps();
    goToOffices(officeList, __current);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::NavigationI::___goToOffice(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    __is->startReadEncaps();
    ::std::string office;
    ::std::string startDate;
    ::std::string startH;
    ::std::string startM;
    ::std::string startP;
    ::std::string endDate;
    ::std::string endH;
    ::std::string endM;
    ::std::string endP;
    __is->read(office);
    __is->read(startDate);
    __is->read(startH);
    __is->read(startM);
    __is->read(startP);
    __is->read(endDate);
    __is->read(endH);
    __is->read(endM);
    __is->read(endP);
    __is->endReadEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = goToOffice(office, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::NavigationI::___escort(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    __is->startReadEncaps();
    ::std::string room;
    ::std::string startDate;
    ::std::string startH;
    ::std::string startM;
    ::std::string startP;
    ::std::string endDate;
    ::std::string endH;
    ::std::string endM;
    ::std::string endP;
    __is->read(room);
    __is->read(startDate);
    __is->read(startH);
    __is->read(startM);
    __is->read(startP);
    __is->read(endDate);
    __is->read(endH);
    __is->read(endM);
    __is->read(endP);
    __is->endReadEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::std::string __ret = escort(room, startDate, startH, startM, startP, endDate, endH, endM, endP, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}

::Ice::DispatchStatus
CMU::NavigationI::___cancelTask(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Normal, __current.mode);
    ::IceInternal::BasicStream* __is = __inS.is();
    __is->startReadEncaps();
    ::std::string taskId;
    __is->read(taskId);
    __is->endReadEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::Ice::Int __ret = cancelTask(taskId, __current);
    __os->write(__ret);
    return ::Ice::DispatchOK;
}

static ::std::string __CMU__NavigationI_all[] =
{
    "cancelTask",
    "escort",
    "goToOffice",
    "goToOffices",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping",
    "whereAreYou"
};

::Ice::DispatchStatus
CMU::NavigationI::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__CMU__NavigationI_all, __CMU__NavigationI_all + 9, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __CMU__NavigationI_all)
    {
        case 0:
        {
            return ___cancelTask(in, current);
        }
        case 1:
        {
            return ___escort(in, current);
        }
        case 2:
        {
            return ___goToOffice(in, current);
        }
        case 3:
        {
            return ___goToOffices(in, current);
        }
        case 4:
        {
            return ___ice_id(in, current);
        }
        case 5:
        {
            return ___ice_ids(in, current);
        }
        case 6:
        {
            return ___ice_isA(in, current);
        }
        case 7:
        {
            return ___ice_ping(in, current);
        }
        case 8:
        {
            return ___whereAreYou(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
CMU::NavigationI::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__write(__os);
#else
    ::Ice::Object::__write(__os);
#endif
}

void
CMU::NavigationI::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__read(__is, true);
#else
    ::Ice::Object::__read(__is, true);
#endif
}

void
CMU::NavigationI::__write(const ::Ice::OutputStreamPtr&) const
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type CMU::NavigationI was not generated with stream support";
    throw ex;
}

void
CMU::NavigationI::__read(const ::Ice::InputStreamPtr&, bool)
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type CMU::NavigationI was not generated with stream support";
    throw ex;
}

void 
CMU::__patch__NavigationIPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::CMU::NavigationIPtr* p = static_cast< ::CMU::NavigationIPtr*>(__addr);
    assert(p);
    *p = ::CMU::NavigationIPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::CMU::NavigationI::ice_staticId(), v->ice_id());
    }
}

bool
CMU::operator==(const ::CMU::NavigationI& l, const ::CMU::NavigationI& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
CMU::operator<(const ::CMU::NavigationI& l, const ::CMU::NavigationI& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}
