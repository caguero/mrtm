//========================================================================
/*!
\file    SchedulerProxy.cpp
\brief   Class that communicates with the web server to schedule tasks on CoBot. It uses curl for HTTP comms.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#include "SchedulerProxy.h"

SchedulerProxy::SchedulerProxy() {
	curl_global_init(CURL_GLOBAL_ALL);
}

SchedulerProxy::~SchedulerProxy() {

}

void
SchedulerProxy::setUser( string user )
{
	this->_user = user;
}

string
SchedulerProxy::getUser()
{
	return _user;
}

void
SchedulerProxy::setPasswd( string passwd )
{
	this->_passwd = passwd;
}

string
SchedulerProxy::getPasswd()
{
	return _passwd;
}

void
SchedulerProxy::setCookie( string cookie )
{
	this->_cookie = cookie;
}

string
SchedulerProxy::getCookie()
{
	return _cookie;
}

void
SchedulerProxy::setBaseURL( string url)
{
	this->_baseURL = url;
}

string
SchedulerProxy::getBaseURL()
{
	return _baseURL;
}

int
writer(char *data, size_t size, size_t nmemb, string *buffer){
	int result = 0;
	if(buffer != NULL) {
		buffer -> append(data, size * nmemb);
		result = size * nmemb;
	}
	return result;
}

string
SchedulerProxy::newTaskEscort( string room, string startDate, string startH, string startM, string startP,
		string endDate, string endH, string endM, string endP )
{
	reply = "";
	string task_id = "NoId";
	reply = "";

	curl = curl_easy_init();

	if(curl) {
		/* what URL that receives this POST */
		curl_easy_setopt(curl, CURLOPT_URL, "http://neontetra.coral.cs.cmu.edu/cobot/dashboard/scheduler1/submit/");
		string data = "task=Escort&room=" + room + "&model=w&date1=" + startDate + "&time1h=" + startH + "&time1m=" + startM + "&time1p=" + startP +
				"&date2=" + endDate + "&time2h=" + endH + "&time2m=" + endM + "&time2p=" + endP;
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);	/* Function Pointer "writer" manages the required buffer size */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &reply ); 	/* Data Pointer &buffer stores downloaded web content */
		curl_easy_setopt(curl, CURLOPT_COOKIE, getCookie().c_str());
		res = curl_easy_perform(curl);

		//cout << reply << endl;

		task_id = getTaskId( reply );

		/* always cleanup */
		curl_easy_cleanup(curl);
	}

	return task_id;
}

string
SchedulerProxy::getTaskId(string httpResponse)
{
	string taskId;
	string startCancelPattern = "/cobot/dashboard/bookings/delete/";
	string endCancelPattern = "/\">Cancel your request</a>";
	size_t taskIdStartPos = httpResponse.find(startCancelPattern) + startCancelPattern.length();
	size_t taskIdEndPos = httpResponse.find(endCancelPattern);
	size_t taskIdLength = taskIdEndPos - taskIdStartPos;
	if (httpResponse.find(startCancelPattern) != string::npos) {
		taskId = httpResponse.substr(taskIdStartPos, taskIdLength);
		//cout << "(" << taskId << ")\n";
		return taskId;
	} else
		return "NoId";
}

//ToDo: Comprobar que la tarea se ha eliminado
int
SchedulerProxy::cancelTask( string taskId )
{
	reply = "";
	string cancelTask_URL = getBaseURL() + "delete/" + taskId + "/";

	curl = curl_easy_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, cancelTask_URL.c_str());
		curl_easy_setopt(curl, CURLOPT_COOKIE, getCookie().c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writer);	/* Function Pointer "writer" manages the required buffer size */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &reply ); 	/* Data Pointer &buffer stores downloaded web content */
		res = curl_easy_perform(curl);

		/* always cleanup */
		curl_easy_cleanup(curl);

		return CANCEL_TASK_SUCCESS;
	}

	return CANCEL_TASK_FAIL;
}

int
SchedulerProxy::cancelLastTask()
{
	return CANCEL_TASK_FAIL;
}

/*int
main(int argc, char* argv[])
{
	SchedulerProxy *cobotProxy = new SchedulerProxy();
	cobotProxy->setUser("tank_cobot");
	cobotProxy->setPasswd("tank_cobot_passwd");
	cobotProxy->setCookie("__utma=136847266.450150512.1315413292.1316658085.1316974809.3; __utmz=136847266.1316974809.3.3.utmcsr=prashantreddy.net|utmccn=(referral)|utmcmd=referral|utmcct=/; sessionid=d442dd7485c45fb46d59e8d11b38970b; __utma=44984886.1366009676.1317254055.1317570952.1317598677.4; __utmz=44984886.1317598704.4.5.utmccn=(organic)|utmcsr=search|utmctr=kanade|utmcmd=organic; webstats-cmu=cmu128.237.241.28.74061317774344638");
	cobotProxy->setBaseURL("http://neontetra.coral.cs.cmu.edu/cobot/dashboard/bookings/");

	string id = cobotProxy->newTaskEscort("F7412", "2011-10-06", "11", "20", "1", "2011-10-06", "11", "30", "1");

	if ( id != "NoId" ) {
		if (cobotProxy->cancelTask( id ) == SchedulerProxy::CANCEL_TASK_FAIL)
			cout << "Sorry, task not canceled\n";
	} else {
		cout << "Sorry, task not booked\n";
	}
}*/
