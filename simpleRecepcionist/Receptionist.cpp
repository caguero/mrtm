//========================================================================
/*!
\file    Receptionsit.cpp
\brief   Class that represents the list of operations to be performed by a receptionist.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#include "Receptionist.h"

Receptionist::Receptionist(int argc, char* argv[]) {
	mrsm = MRSMreceptionist::getInstance();
	mrsm->init(argc, argv);
}

Receptionist::~Receptionist() {

}

void
Receptionist::detailedEscortMenu(string &room, string &startingDate, string &hour,
		string &minute, string &meridiem ) {

	cout << string(50, '\n');
	cout << " ======= ESCORT A PERSON =======\n\n\n";

	cout << "Enter room number (e.g. F7412): ";
	cin >> room;
	cout << "Enter the starting day of the task (yyyy-mm-dd): ";
	cin >> startingDate;
	cout << "Enter the hour of the task (hh): ";
	cin >> hour;
	cout << "Enter the minute of the task (mm): ";
	cin >> minute;
	cout << "Enter the meridiem of the task (0 for AM / 1 for PM): ";
	cin >> meridiem;
}

Receptionist::tOption
Receptionist::showMainMenu() {
	int option;

	do {
		cout << string(50, '\n');
		cout << "\t\t\t\t\t\t\t ======= PEOPLE DIRECTORY =======\n";

		//cout << "\n\n\t\t\t\t\t\t\t > Request received: Which is the office of Person 2?\n";
		//cout << "\t\t\t\t\t\t\t > Answer sent: F7412\n";
		cout << "\t\t\t\t\t\t\t 0.- People directory\n";
		cout << "\t\t\t\t\t\t\t 1.- Exit\n";

		cout << "\t\t\t\t\t\t\t Please, enter a valid option [0-" << Receptionist::EXIT << "]: ";
		cin >> option;
	} while ((option < 0) || (option > Receptionist::EXIT));

	return (Receptionist::tOption)option;
}

int
Receptionist::mainEscortMenu() {
	int option;

	do {
		cout << "\t\t\t\t\t\t\t ======= ESCORT A PERSON =======\n\n\n";
		cout << "\t\t\t\t\t\t\t Would you like to be escorted by CoBot?\n";
		cout << "\t\t\t\t\t\t\t 0.- No\n";
		cout << "\t\t\t\t\t\t\t 1.- Yes\n";

		cout << "\t\t\t\t\t\t\t Please, enter a valid option [0-1]: ";
		cin >> option;
	} while ((option < 0) || (option > 1));

	return option;
}

string
Receptionist::showOfficesAndPeopleMenu() {
	int option;
	string room = "Unknown";

	do {
		cout << string(50, '\n');
		cout << "\t\t\t\t\t\t\t ======= PEOPLE DIRECTORY =======\n\n\n";
		cout << "\t\t\t\t\t\t\t Select the name of the person to know his/her office?\n";
		cout << "\t\t\t\t\t\t\t 0.- Manuela Veloso\n";
		cout << "\t\t\t\t\t\t\t 1.- Reid Simmons\n";
		cout << "\t\t\t\t\t\t\t 2.- Carlos Agüero\n";
		cout << "\t\t\t\t\t\t\t 3.- CORAL Robot Lab\n";

		cout << "\t\t\t\t\t\t\t Enter a valid option [0-2]: ";
		cin >> option;
	} while ((option < 0) || (option > 3));

	switch (option) {
	case 0:
		room = "F7002";
		cout << "\t\t\t\t\t\t\t =================================================\n";
		cout << "\t\t\t\t\t\t\t Manuela Veloso's office is 7002 at Gates building\n";
		cout << "\t\t\t\t\t\t\t =================================================\n";
		break;
	case 1:
		cout << "\t\t\t\t\t\t\t ==================================================\n";
		cout << "\t\t\t\t\t\t\t Reid Simmons's office is 3213 at Newell-Simon Hall\n";
		cout << "\t\t\t\t\t\t\t ==================================================\n";
		break;
	case 2:
		room = "F9008";
		cout << "\t\t\t\t\t\t\t ================================================\n";
		cout << "\t\t\t\t\t\t\t Carlos Agüero's office is 9008 at Gates building\n";
		cout << "\t\t\t\t\t\t\t ================================================\n";
		break;
	case 3:
		room = "F7412";
		cout << "\t\t\t\t\t\t\t =========================================\n";
		cout << "\t\t\t\t\t\t\t CORAL Robot Lab is 7412 at Gates building\n";
		cout << "\t\t\t\t\t\t\t =========================================\n";
		break;
	default:
		cout << "\t\t\t\t\t\t\t Wrong option\n";
	}

	// Reserve a task for Gates offices
	if ( (room.compare("Unknown") != 0) && (room.at(0) == 'F') ) {
		//string id = mrsm->escort( room, "2011-10-26", "08", "00", "1",
		//				"2011-10-26", "08", "00", "1" );

		//cerr << "escort()\n";

		string id = mrsm->escort( room, "2011-11-10", "09", "00", "1",
								"2011-11-10", "09", "01", "1" );

		if (id != "NoId") { // Booking success

			// Inform the visitor about the possibility of be escorted
			option = mainEscortMenu();

			if (option == 1) { 	// Detailed information about the Escort task
				cout << "\t\t\t\t\t\t\t ===============================================================================\n";
				cout << "\t\t\t\t\t\t\t Great! CoBot will be waiting in 5' in front of the elevator at the " <<
						room[1] << " floor\n";
				cout << "\t\t\t\t\t\t\t ===============================================================================\n";
			} else {			// Cancel the booking
				mrsm->cancelTask( id );
			}
		}
	}

	return room;
}

int
main(int argc, char* argv[])
{
	Receptionist tankJr( argc, argv );

	Receptionist::tOption option = tankJr.showMainMenu();
	string room, sDate, hour, minute, meridiam;

	/*time_facet *facet = new time_facet("%Y-%m-%d %I:%M:%P");
	cout.imbue(locale(cout.getloc(), facet));
	facet->
	cout << second_clock::local_time() << endl;*/

	switch (option) {
	case 0:
		room = tankJr.showOfficesAndPeopleMenu();
		break;
	default:
		break;
	}
}
