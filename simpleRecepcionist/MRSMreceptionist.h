//========================================================================
/*!
\file    MRSMreceptionsit.h
\brief   MRSM for Nao. MRSM is a distributed module and this is the module that runs on Nao.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#ifndef MRSMreceptionist_H_
#define MRSMreceptionist_H_

// Interfaces available among all robots
#include "AgendaI.h"
#include "NavigationI.h"

#include <Ice/Ice.h>
#include <IceUtil/IceUtil.h>
#include <pthread.h>
#include "Singleton.h"

using namespace std;

class MRSMreceptionist : public Singleton<MRSMreceptionist>, public CMU::AgendaI {
public:
	static const int ICE_PORT = 10111;

	MRSMreceptionist();
	virtual ~MRSMreceptionist();

	void init(int argc, char **argv);
	string officeOf( const string& personName );
	string escort( string room, string startDate, string startH, string startM, string startP,
			string endDate, string endH, string endM, string endP );
	string goToOffice( string room, string startDate, string startH, string startM, string startP,
				string endDate, string endH, string endM, string endP );
	int cancelTask( string taskId );

	virtual string officeOf( const string& personName, const Ice::Current& c );
	static void* iceServerThread(void *obj);
private:
	int myIcePort;
	pthread_t tIceServer;
	Ice::CommunicatorPtr ic;
};

#endif /* MRSMreceptionist_H_ */
