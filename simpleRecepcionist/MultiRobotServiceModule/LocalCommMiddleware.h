/**
 * LocalCommMiddleware.h
 *
 *  Created on: 07/10/2011
 *      Author: Carlos Agüero
 *
 * Interface for the Local Communication Middleware (LCM) component. (C++ <-> C++)
 */

class LocalCommMiddleware {
public:
  /**
   * Default constructor (empty since this is an interface).
   */
  LocalCommMiddleware() {};

  /**
   * Destructor (empty since this is an interface).
   */
  virtual ~LocalCommMiddleware() {};

  /**
   * Runs a new high level task
   *
   * @param task the task to be remotely executed
   *
   * @return whether the task is succesfully scheduled (0) or not (any other error code)
   */
  virtual int runTask( string task ) = 0;
};
