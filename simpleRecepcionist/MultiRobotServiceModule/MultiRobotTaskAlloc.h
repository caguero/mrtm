/*
 * MultiRobotTaskAlloc.h
 *
 *  Created on: 07/10/2011
 *      Author: Carlos Agüero
 *
 *  Interface for the Multi-Robot Task Allocation (MRTA) module
 *
 */

class MultiRobotTaskAlloc {
public:
  /**
   * Default constructor (empty since this is an interface).
   */
	MultiRobotTaskAlloc() {};

  /**
   * Destructor (empty since this is an interface).
   */
  virtual ~MultiRobotTaskAlloc() {};

  /**
   * Schedule a set of tasks using other robots
   *
   * @param tasks the list of subtasks to be scheduled
   *
   * @return whether all the subtasks has been scheduled (0) or not (other error code)
   */
  virtual int scheduleTasks( vector<string> tasks ) = 0;
};
