/*
 * MultiRobotTP.h
 *
 *  Created on: 07/10/2011
 *      Author: Carlos Agüero
 *
 *  Interface for the Multi-Robot Task Planner (MRTP)
 *
 */


class MultiRobotTP {
public:
  /**
   * Default constructor (empty since this is an interface).
   */
	MultiRobotTP() {};

  /**
   * Destructor (empty since this is an interface).
   */
  virtual ~MultiRobotTP() {};

  /**
   * Decompose a high level task into sub-tasks
   *
   * @param task the task to be remotely executed
   *
   * @return a list of subtasks to be performed. Each subtask is codified as a
   * different string linked into a list
   */
  virtual vector<string> planTask( string task ) = 0;
};
