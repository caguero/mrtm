//========================================================================
/*!
\file    Receptionsit.h
\brief   Class that represents the list of operations to be performed by a receptionist.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#ifndef RECEPTIONIST_H_
#define RECEPTIONIST_H_

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include "SchedulerProxy.h"
#include "MRSMreceptionist.h"

using namespace std;
using namespace boost::posix_time;

class Receptionist {
public:
	enum tOption {
			ESCORT,
			EXIT	// Always let this item at the end!
	};

	Receptionist(int argc, char* argv[]);
	virtual ~Receptionist();
	tOption showMainMenu();
	int mainEscortMenu();
	void detailedEscortMenu(string &room, string &startingDate, string &hour,
			string &minute, string &meridiem );
	string showOfficesAndPeopleMenu();
	SchedulerProxy* getScheduler();

private:
	SchedulerProxy 		*cobotScheduler;
	MRSMreceptionist	*mrsm;
};

#endif /* RECEPTIONIST_H_ */
