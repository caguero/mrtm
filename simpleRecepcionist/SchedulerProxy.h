//========================================================================
/*!
\file    SchedulerProxy.h
\brief   Class that communicates with the web server to schedule tasks on CoBot. It uses curl for HTTP comms.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================


#ifndef SCHEDULERPROXY_H_
#define SCHEDULERPROXY_H_

#include <string>
#include <vector>
#include <curl/curl.h>
#include <iostream>

using namespace std;

class SchedulerProxy {
public:
	static const int CANCEL_TASK_SUCCESS = 0;
	static const int CANCEL_TASK_FAIL = 1;

	//ToDo: Read user/passwd from a configuration file
	SchedulerProxy();
	virtual ~SchedulerProxy();

	void setUser( string user);
	string getUser();
	void setPasswd( string );
	string getPasswd();
	void setCookie( string );
	string getCookie();
	void setBaseURL( string );
	string getBaseURL();

	// Task manage
	string newTaskEscort( string room, string startDate, string startH, string startM, string startP,
			string endDate, string endH, string endM, string endP );
	int cancelTask( string taskId );
	int cancelLastTask();

private:
	string getTaskId( string httpResponse );

	string _user;
	string _passwd;
	string _cookie;
	string _baseURL;

	// curl
	CURL *curl;
	CURLcode res;
	string reply;
};

#endif /* SCHEDULERPROXY_H_ */
