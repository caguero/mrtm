//========================================================================
/*!
\file    MRSMreceptionsit.cpp
\brief   MRSM for Nao. MRSM is a distributed module and this is the module that runs on Nao.
\author  Carlos Agüero (caguero@gsyc.es), 2011
 */
//========================================================================

#include "MRSMreceptionist.h"

MRSMreceptionist::MRSMreceptionist() {
	myIcePort = ICE_PORT;
}

MRSMreceptionist::~MRSMreceptionist() {
	// TODO Auto-generated destructor stub
}

void
MRSMreceptionist::init(int argc, char **argv)
{
	static int status = 0;
	try {
		ic = Ice::initialize(argc, argv);
		pthread_create(&tIceServer, NULL, iceServerThread, &ic);
	} catch (const Ice::Exception& e) {
		cerr << e << endl;
		status = 1;
	} catch (const char* msg) {
		cerr << msg << endl;
		status = 1;
	}
}

string
MRSMreceptionist::officeOf( const string& personName )
{
	if (personName.compare("Manuela Veloso") == 0)
		return "F7002";
	else if (personName.compare("Reid Simmons") == 0)
		return "NSH3212";
	else if (personName.compare("Carlos Agüero") == 0)
		return "F9008";
	else if (personName.compare("CORAL Robot Lab") == 0)
		return "F7412";
	else
		return "Unknown";
}

string
MRSMreceptionist::escort( string room, string startDate, string startH, string startM, string startP,
		string endDate, string endH, string endM, string endP )
{
	string taskId = "NoId";

	try {
		Ice::ObjectPrx proxy = ic->stringToProxy("NavigationI@cobot2");
		CMU::NavigationIPrx navigationPrx = CMU::NavigationIPrx::checkedCast(proxy);
		if (!navigationPrx)
			throw "Invalid proxy";

		taskId = navigationPrx->escort( room, startDate, startH, startM, startP,
				endDate, endH, endM, endP );

	} catch (const Ice::Exception& ex) {
		cerr << ex << endl;
	} catch (const char* msg) {
		cerr << msg << endl;
	}

	return taskId;
}

string
MRSMreceptionist::goToOffice( string room, string startDate, string startH, string startM, string startP,
		string endDate, string endH, string endM, string endP )
{
	string taskId = "NoId";

	try {
		Ice::ObjectPrx proxy = ic->stringToProxy("NavigationI@cobot2");
		CMU::NavigationIPrx navigationPrx = CMU::NavigationIPrx::checkedCast(proxy);
		if (!navigationPrx)
			throw "Invalid proxy";

		taskId = navigationPrx->goToOffice( room, startDate, startH, startM, startP, endDate, endH, endM, endP );

	} catch (const Ice::Exception& ex) {
		cerr << ex << endl;
	} catch (const char* msg) {
		cerr << msg << endl;
	}

	return taskId;
}

int
MRSMreceptionist::cancelTask( string taskId )
{
	int status = 0;
	try {
		Ice::ObjectPrx proxy = ic->stringToProxy("NavigationI@cobot2");
		CMU::NavigationIPrx navigationPrx = CMU::NavigationIPrx::checkedCast(proxy);

		if (!navigationPrx)
			throw "Invalid proxy";

		taskId = navigationPrx->cancelTask( taskId );

	} catch (const Ice::Exception& ex) {
		cerr << ex << endl;
		status = 1;
	} catch (const char* msg) {
		cerr << msg << endl;
		status = 1;
	}
	return 0;
}

string
MRSMreceptionist::officeOf( const string& personName, const Ice::Current& c )
{
	return officeOf( personName );
}

void*
MRSMreceptionist::iceServerThread(void *obj)
{
	static int status = 0;
	Ice::CommunicatorPtr ic = *((Ice::CommunicatorPtr*)obj);
	try {

		Ice::ObjectAdapterPtr adapter = ic->createObjectAdapter("CMUreceptionistAdapter");

		adapter->add(getInstance(), ic->stringToIdentity("AgendaI"));
		adapter->activate();
		ic->waitForShutdown();
	} catch (const Ice::Exception& e) {
		cerr << e << endl;
		status = 1;
	} catch (const char* msg) {
		cerr << msg << endl;
		status = 1;
	}
	return &status;
}
